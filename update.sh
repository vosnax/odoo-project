#!/bin/bash
docker run -it --rm -v /home/llacroix/odoo-project/addons:/addons -v docker-sass-addons:/addons2 debian:stretch find /addons2 -mindepth 1 -delete
docker run -it --rm -v /home/llacroix/odoo-project/addons:/addons -v docker-sass-addons:/addons2 debian:stretch cp -r /addons/. /addons2
docker service update --force odoo

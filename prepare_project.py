# -*- coding: utf-8 -*-
import json
from subprocess import call as run
from os import listdir, mkdir
from os import path

filename = 'package.json'
dockerfile = open("Dockerfile", "w")

dockerfile.write("From odoo:12.0\n")
dockerfile.write("User root\n")
dockerfile.write("Run groupadd -g 119 docker\n")
dockerfile.write("Run usermod -a -G docker odoo\n")
dockerfile.write("Run mkdir -p /var/lib/apt/lists/partial\n")
dockerfile.write("Run apt-get clean\n")
dockerfile.write("Run apt-get update\n")
dockerfile.write("Run apt-get install -y python-pip git\n")

with open(filename) as config:
    config = json.load(config)


version = config['version']

for package in config['packages']:
    dockerfile.write("Run pip3 install %s\n" % package)

for repo in config['dependencies']:
    if repo.startswith('local'):
        handler, command = repo.split(' ', 1)
        dockerfile.write("copy %s /mnt/extra-addons\n" % command)
    else:
        project_folder = path.join('fetch', path.basename(repo).replace('.git', ''))
        run(["git", "clone", "--depth", "1", "-b", version, repo, project_folder])
        dockerfile.write("copy ./%s /mnt/extra-addons\n" % project_folder)

dockerfile.write('RUN sed -i -e "s/; dbfilter.*/dbfilter = production/g" /etc/odoo/odoo.conf\n')

dockerfile.write("user odoo\n")
dockerfile.write('ENTRYPOINT ["/entrypoint.sh"]\n')
dockerfile.write('CMD ["odoo"]\n')

dockerfile.close()

# -*- coding: utf-8 -*-
import lzma
from odoo import http, models, fields, _
from odoo.http import request, Response
from odoo.tools import pycompat, OrderedSet
from odoo.tools.config import config
from odoo.addons.http_routing.models.ir_http import slug, _guess_mimetype
from odoo.addons.web.controllers.main import WebClient, Binary
from odoo.addons.portal.controllers.portal import pager as portal_pager
from odoo.addons.portal.controllers.web import Home

# from pretty_bad_protocol import gnupg
# import gnupg

from base64 import b64decode
import os
from ..models.debian_arch import SECRET

os.environ['GPG_AGENT_INFO'] = ''


class Website(Home):

    @http.route(
        '/gpg/<fingerprint>.key',
        type='http',
        auth="public",
        website=True
    )
    def get_dist_release(self, fingerprint, *args, **kargs):
        key_obj = request.env['gpg.key']
        key = key_obj.sudo().search([
            ['name', '=', fingerprint],
        ])
        return Response(
            key.public_key,
            content_type='text/plain; charset=utf-8',
        )

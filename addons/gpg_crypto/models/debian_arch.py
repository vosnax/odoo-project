# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.config import config
from os import path
import os

import lzma
import gzip
from collections import defaultdict
from datetime import datetime
import pytz
from io import BytesIO

from pretty_bad_protocol import gnupg
# import gnupg

SECRET = "SOMESECRETPASSPHRASE"

os.environ['GPG_AGENT_INFO'] = ''


class GPGKey(models.Model):
    _name = 'gpg.key'

    name = fields.Char(
        string="name",
        readonly=True
    )
    public_key = fields.Text(
        compute='compute_public_key',
        string="Public Key",
        readonly=True,
        store=True
    )
    partner_id = fields.Many2one(
        'res.partner',
        string="Partner",
        required=True
    )
    email = fields.Char(related="partner_id.email")
    name_real = fields.Char(related="partner_id.name")

    @api.model
    def create(self, vals):
        res = super(GPGKey, self).create(vals)
        gpg = gnupg.GPG(homedir=config.options['data_dir'])
        params = {
            'name_real': res.name_real,
            'name_email': res.email,
            'passphrase': SECRET,
            'key_length': 2048,
        }
        batch_key_input = gpg.gen_key_input(**params)
        key = gpg.gen_key(batch_key_input)
        res.name = key.fingerprint
        return res

    @api.multi
    def get_gpg(self):
        gpg = gnupg.GPG(
            homedir=config.options['data_dir'],
            use_agent=False,
            options=['--pinentry-mode loopback']
        )
        return gpg

    @api.multi
    @api.depends('name')
    def compute_public_key(self):
        for obj in self:

            if not obj.name:
                continue

            gpg = obj.get_gpg()
            obj.public_key = gpg.export_keys(obj.name)

    @api.multi
    def sign(self, data, detach=False):
        self.ensure_one()
        gpg = self.get_gpg()
        clearsign = True

        if detach:
            clearsign = False

        signature = gpg.sign(
            data,
            default_key=self.name,
            passphrase=SECRET,
            clearsign=clearsign,
            detach=detach
        )

        return signature.data

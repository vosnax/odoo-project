# -*- coding: utf-8 -*-
from odoo import http, models, fields, _
from odoo.http import request
from odoo.addons.web.controllers.main import Home

import logging

log = logging.getLogger(__name__)


class Website(Home):
    @http.route(
        '/git/webhooks/gitlab',
        type='json',
        auth="public",
        website=True,
        methods=['GET', 'POST'],
        csrf=False
    )
    def webhook_endpoint_gitlab(self, *args, **kwargs):
        # hr = request.httprequest
        log.info(request.jsonrequest)
        data = request.jsonrequest
        # Dispatch the request to the main event handler
        res = request.env['git.event'].sudo()
        res = res.with_context(GITLAB_WEBHOOK=True)
        res.handle_request('gitlab', data)

        return res or 'ok'

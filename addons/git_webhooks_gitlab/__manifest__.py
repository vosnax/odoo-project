# -*- coding: utf-8 -*-
{
    'name': 'Git Webhooks Gitlab',
    'version': '11.0.0.1.0',
    'author': 'Loïc Faure-Lacroix',
    'maintainer': '',
    'website': 'http://archeti.ca',
    'license': 'AGPL-3',
    'category': 'Others',
    'summary': '',
    'description': """
""",
    'depends': [
        'web',
        'git_webhooks',
    ],
    'external_dependencies': {
        'python': [],
    },
    'data': [
    ],
    'installable': True,
    'application': True,
}

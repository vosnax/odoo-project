# -*- coding: utf-8 -*-
from odoo import fields, models, api
import logging

log = logging.getLogger(__name__)


class GitEventPush(models.Model):
    _inherit = 'git.event.push'

    @api.model
    def _prepare_from_gitlab(self, data):
        return {
            'backend': 'gitlab',
            'type': 'push',
            'user_name': data['user_name'],
            'user_username': data['user_username'],
            'ref': data['ref'],
            'before_commit_id': data['before'],
            'after_commit_id': data['after'],
        }

    def handle_request(self, backend, data):
        if not backend == 'gitlab':
            return super(GitEventPush, self).handle_request(backend, data)

        log.info('In event gitlab handle_request push')

        push_obj = self.env['git.event.push']
        project_obj = self.env['git.project']

        # Load project data if any and update
        git_project = project_obj.search([
            ['gitlab_id', '=', data.get('project_id')],
        ])
        if not git_project:
            git_project = project_obj.create_from_gitlab(data.get('project'))
        else:
            git_project.write(
                project_obj._prepare_from_gitlab(data.get('project'))
            )

        vals = self._prepare_from_gitlab(data)
        vals['project_id'] = git_project.id

        res = push_obj.create(vals)

        return res

    @api.multi
    def name_get(self):
        gitlab_ids = self.filtered(lambda x: x.backend == 'gitlab')
        rest = (self - gitlab_ids)

        names = super(GitEventPush, rest).name_get()

        for record in gitlab_ids:
            names.append(
                (
                    record.id,
                    '%s -> %s' % (record.user_username, record.ref)
                )
            )

        return names

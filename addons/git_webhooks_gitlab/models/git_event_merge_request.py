# -*- coding: utf-8 -*-
from odoo import fields, models, api
import logging

log = logging.getLogger(__name__)


class GitEventMergeRequest(models.Model):
    _inherit = 'git.event.merge_request'

    gitlab_id = fields.Integer(string="ID")
    gitlab_iid = fields.Integer(string="IID")

    @api.multi
    def compute_event_url(self):
        obj_ids = self.filtered(lambda x: x.backend == 'gitlab')
        rest = (self - obj_ids)

        super(GitEventMergeRequest, rest).compute_event_url()

        for obj in obj_ids:
            obj.event_url = '/'.join([
                obj.source_project_id.url,
                'merge_requests',
                str(obj.gitlab_iid)
            ])

    @api.model
    def _prepare_from_gitlab(self, data):

        state_code = data['object_attributes']['state']

        stage = self.env['git.event.merge_request.stage'].search([
            ['code', '=', state_code]
        ], limit=1)

        if (
            stage.code == 'opened' and
            not data['object_attributes']['title'].startswith('WIP:')
        ):
            stage = self.env['git.event.merge_request.stage'].search([
                ['code', '=', 'ready']
            ], limit=1)

        return {
            'backend': 'gitlab',
            'type': 'merge_request',
            'source_branch': data['object_attributes']['source_branch'],
            'target_branch': data['object_attributes']['target_branch'],
            'user_username': data['user']['username'],
            'user_name': data['user']['name'],
            'title': data['object_attributes']['title'],
            'gitlab_id': data['object_attributes']['id'],
            'gitlab_iid': data['object_attributes']['iid'],
            'stage_id': stage.id,
            'last_commit': data['object_attributes']['last_commit']['id'],
            'kanban_stage': 'new'
        }

    @api.multi
    def get_project(self, project_dict, target):
        project_obj = self.env['git.project']

        data = project_dict[target]
        data['id'] = project_dict['%s_project_id' % target]
        git_project = project_obj.search([
            ['gitlab_id', '=', data.get('id')],
        ])
        if not git_project:
            git_project = project_obj.create_from_gitlab(data)
        else:
            git_project.write(
                project_obj._prepare_from_gitlab(data)
            )
        return git_project

    def handle_request(self, backend, data):
        if not backend == 'gitlab':
            return super(GitEventMergeRequest, self).handle_request(
                backend, data
            )

        log.info('In event gitlab handle_request merge_request')

        source_project = self.get_project(data['object_attributes'], 'source')
        target_project = self.get_project(data['object_attributes'], 'target')
        # Load project data if any and update

        vals = self._prepare_from_gitlab(data)
        vals['source_project_id'] = source_project.id
        vals['target_project_id'] = target_project.id

        merge_id = data['object_attributes']['id']
        merge_object = self.search([['gitlab_id', '=', merge_id]])

        if merge_object:
            merge_object.write(vals)
        else:
            merge_object = self.create(vals)

        return merge_object

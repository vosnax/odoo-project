# -*- coding: utf-8 -*-
import logging
from odoo import api, models, fields

log = logging.getLogger(__name__)


class GitEvent(models.Model):

    _inherit = 'git.event'

    backend = fields.Selection(
        selection_add=[
            ('gitlab', 'Gitlab'),
        ]
    )

    @api.model
    def handle_request(self, backend, data):
        if not backend == 'gitlab':
            return super(GitEvent, self).handle_request(backend, data)

        log.info('In event gitlab handle_request')

        obj_type = data.get('object_kind', None)
        if not obj_type:
            return

        model = self.env['git.event.%s' % obj_type]
        return model.handle_request(backend, data)

# -*- coding: utf-8 -*-
from odoo import api, models, fields


class GitProject(models.Model):

    _inherit = 'git.project'

    backend = fields.Selection(
        selection_add=[
            ('gitlab', 'Gitlab'),
        ]
    )

    gitlab_id = fields.Integer(string="Gitlab ID")

    @api.model
    def _prepare_from_gitlab(self, data):
        return {
            'backend': 'gitlab',
            'name': data['name'],
            'description': data['description'],
            'ssh_url': data['ssh_url'],
            'http_url': data['http_url'],
            'url': data['web_url'],
            'gitlab_id': data['id'],
            'default_branch': data['default_branch'],
        }

    @api.model
    def create_from_gitlab(self, data):
        return self.create(self._prepare_from_gitlab(data))

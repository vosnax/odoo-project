# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
import logging

log = logging.getLogger(__name__)


class GitMergeRequestStage(models.Model):
    _name = 'git.event.merge_request.stage'
    _order = 'sequence'

    sequence = fields.Integer(string="Sequence")
    name = fields.Char(string="Name")
    code = fields.Char(string="Code")
    fold = fields.Boolean(string="Fold")

    legend_normal = fields.Char(string="Legend Normal")
    legend_done = fields.Char(string="Legend Done")
    legend_blocked = fields.Char(string="Legend Blocked")


class GitEventMergeRequest(models.Model):
    _name = 'git.event.merge_request'

    _inherits = {
        'git.event': 'event_id',
    }

    name = fields.Char(compute="compute_name", string="Name")
    title = fields.Char(string="title")

    event_id = fields.Many2one(
        'git.event',
        string="Event",
        ondelete='cascade',
        required=True
    )

    stage_id = fields.Many2one(
        'git.event.merge_request.stage',
        string="Stage",
        group_expand='_read_group_stage_ids'
    )
    kanban_state = fields.Selection(
        selection=[
            ('normal', _('Pending')),
            ('done', _('Done')),
            ('blocked', _('Blocked')),
        ],
        string="State",
        default="normal",
    )
    color = fields.Integer(string='Color Index')

    source_branch = fields.Char(string="Source Branch")
    target_branch = fields.Char(string="Target Branch")
    last_commit = fields.Char(string="Last Commit")

    commit_ids = fields.Many2many(
        'git.commit',
        string="Commits"
    )

    source_project_id = fields.Many2one(
        'git.project',
        string="Project"
    )
    target_project_id = fields.Many2one(
        'git.project',
        string="Project"
    )
    event_url = fields.Char(
        compute="compute_event_url",
        string="Url"
    )

    @api.multi
    def compute_event_url(self):
        for obj in self:
            obj.event_url = obj.source_project_id.url

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        stage_ids = self.env['git.event.merge_request.stage'].search([])
        return stage_ids

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            result.append(
                (
                    record.id,
                    "[%s -> %s] %s" % (
                        record.source_branch,
                        record.target_branch,
                        record.title
                    )
                )
            )
        return result

    @api.multi
    def compute_name(self):
        for record in self:
            record.name = self.name_get()[0][1]


class GitCommit(models.Model):
    _inherit = 'git.commit'

    merge_request_ids = fields.Many2many(
        'git.event.merge_request',
        string="Merge Event"
    )


class GitProject(models.Model):
    _inherit = 'git.project'

    source_merge_request_ids = fields.One2many(
        'git.event.merge_request',
        'source_project_id',
        string="Merge Events",
    )
    target_merge_request_ids = fields.One2many(
        'git.event.merge_request',
        'target_project_id',
        string="Merge Events",
    )

# -*- coding: utf-8 -*-
from odoo import models, fields, api
import re


class GitProject(models.Model):
    _name = 'git.project'

    backend = fields.Selection(selection=[], string="Backend")
    name = fields.Char(string="Name")
    description = fields.Text(string="Description")
    url = fields.Char(string="Url")
    ssh_url = fields.Char(string='SSH Url')
    http_url = fields.Char(string='HTTP Url')
    default_branch = fields.Char(string="Default Branch")


class GitProjectMergeRule(models.Model):
    _name = 'git.project.merge.rule'
    _order = 'sequence'

    name = fields.Char(string="Name")
    sequence = fields.Integer(string="Sequence")
    to_default = fields.Boolean(string="Use Default")
    custom_branch = fields.Char('Target Branch')
    target_branch = fields.Char(
        compute='compute_branch_to',
        string='Target'
    )
    project_id = fields.Many2one(
        'git.project',
        string="Project"
    )

    @api.multi
    @api.depends('to_default', 'custom_branch')
    def compute_branch_to(self):
        for obj in self:
            if obj.to_default:
                obj.target_branch = obj.project_id.default_branch
            else:
                obj.target_branch = obj.custom_branch


class GitProject2(models.Model):
    _inherit = 'git.project'

    merge_rule_ids = fields.One2many(
        'git.project.merge.rule',
        'project_id',
        string="Merge Rules"
    )

    @api.multi
    def get_target_branch(self, cur_branch):
        self.ensure_one()

        for rule in self.merge_rule_ids:
            rerule = re.compile(rule.name)
            if re.match(rerule, cur_branch):
                return rule.target_branch

# -*- coding: utf-8 -*-
from odoo import api, fields, models


class GitCommit(models.Model):
    _name = 'git.commit'

    _rec_name = 'message'

    commit_id = fields.Char(string='Commit ID')
    message = fields.Char(string="Message")
    date = fields.Datetime(string="TimeStamp")
    url = fields.Char(string="Url")
    author_name = fields.Char(string="Author")
    author_email = fields.Char(string="Email")

    project_id = fields.Many2one('git.project', string="Project")


class GitProject(models.Model):
    _inherit = 'git.project'

    commit_ids = fields.One2many(
        'git.commit',
        'project_id',
        string="Commits"
    )

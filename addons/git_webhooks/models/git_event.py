# -*- coding: utf-8 -*-
from odoo import models, api, fields


class GitEvent(models.Model):
    _name = 'git.event'
    _order = 'create_date desc'

    type = fields.Char(string="Event Type")

    user_name = fields.Char(string="Name")
    user_username = fields.Char(string="Username")

    backend = fields.Selection(
        selection=[],
        string="Backend",
    )

    @api.model
    def handle_request(self, backend, data):
        return None

# -*- coding: utf-8 -*-
from odoo import fields, models, api


class GitEventPush(models.Model):
    _name = 'git.event.push'
    _order = 'create_date desc'

    _inherits = {
        'git.event': 'event_id',
    }

    NULL_COMMIT = '0000000000000000000000000000000000000000'

    name = fields.Char(compute="compute_name", string="Name")

    ref = fields.Char(string="Refs")

    event_id = fields.Many2one(
        'git.event',
        string="Event",
        ondelete='cascade',
        required=True
    )

    commit_ids = fields.Many2many(
        'git.commit',
        string="Commits"
    )

    before_commit_id = fields.Char(string="Before Push")
    after_commit_id = fields.Char(string="After Push")

    project_id = fields.Many2one(
        'git.project',
        string="Project"
    )

    branch_url = fields.Char(
        compute="compute_branch_url",
        string="Branch URL"
    )

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, record.ref))
        return result

    @api.multi
    def compute_name(self):
        for record in self:
            record.name = record.name_get()[0][1]

    @api.multi
    def compute_branch_url(self):
        for record in self:
            record.branch_url = '/'.join(
                [
                    record.project_id.url,
                    'tree',
                    record.ref.rsplit('/', 1)[1]
                ]
            )


class GitCommit(models.Model):
    _inherit = 'git.commit'

    push_ids = fields.Many2many(
        'git.event.push',
        string="Push Event"
    )


class GitProject(models.Model):
    _inherit = 'git.project'

    push_ids = fields.One2many(
        'git.event.push',
        'project_id',
        string="Push Events",
    )

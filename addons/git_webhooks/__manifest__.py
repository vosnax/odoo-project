# -*- coding: utf-8 -*-
{
    'name': 'Git WebHooks',
    'version': '11.0.0.1.0',
    'author': 'Loïc Faure-Lacroix',
    'maintainer': '',
    'website': 'http://archeti.ca',
    'license': 'AGPL-3',
    'category': 'Others',
    'summary': '',
    'description': """
""",
    'depends': [
        'web',
    ],
    'external_dependencies': {
        'python': [],
    },
    'data': [
        'menus/actions.xml',
        'menus/menus.xml',
        'views/views.xml',
        'security/groups.xml',
        'security/acl.xml',
    ],
    'installable': True,
    'application': True,
}

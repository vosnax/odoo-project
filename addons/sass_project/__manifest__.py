# -*- coding: utf-8 -*-
{
    'name': 'Sass Projects',
    'version': '8.0.0.1.0',
    'author': 'Loïc Faure-Lacroix',
    'maintainer': '',
    'website': 'http://archeti.ca',
    'license': 'AGPL-3',
    'category': 'Others',
    'summary': '',
    'description': """
""",
    'depends': [
        'website',
        'docker_hosts',
        'git_webhooks',
        'queue_job',
    ],
    'external_dependencies': {
        'python': [],
    },
    'data': [
        'actions/actions.xml',
        'menus/menus.xml',
        'views/sass_project.xml',
        'views/sass_service_template.xml',
        'views/sass_mount_template.xml',
        'views/sass_project_spec.xml',
        'views/res_partner.xml',
        'views/web_templates.xml',
        'views/ssh_key.xml',
        'security/security.xml',
    ],
    'installable': True,
    'application': True,
}

import odoo
from odoo import api, fields, models
import requests 
from io import BytesIO
from base64 import b64encode
import shutil
import tempfile
import psycopg2
import subprocess
import os
import zipfile
import gzip
import re
from uuid import uuid1

from os import path

from odoo.service.db import DatabaseExists
from odoo.tools.misc import find_pg_tool

import logging
log = logging.getLogger(__name__)


class IrAttachment(models.Model):
    _inherit = 'ir.attachment'

    @api.model
    def _storage(self):
        if self.env.context.get('force_filestore', False):
            return 'file'
        else:
            return super(IrAttachment, self)._storage()

def exec_pg_environ(user, password, host, port):
    env = os.environ.copy()

    env['PGHOST'] = host
    env['PGPORT'] = str(port)
    env['PGUSER'] = user
    env['PGPASSWORD'] = password

    return env

def exec_pg_command(env, name, args, ignore_error=False):
    prog = find_pg_tool(name)
    with open(os.devnull) as dn:
        args2 = [prog] + args
        rc = subprocess.call(args2, env=env, stdout=dn, stderr=subprocess.STDOUT)
        if rc and not ignore_error:
            raise Exception('Postgres subprocess %s error %s' % (args2, rc))

def exec_pg_query(env, name, query):
    cmd = 'psql'
    args = [
        '-q',
        '-d', name,
        '-c',
        query
    ]
    exec_pg_command(env, cmd, args, ignore_error=False)

class SassProjectDatabase(models.Model):

    _name = 'sass.project.database'

    name = fields.Char(
        string="Name",
        required=True
    )

    fetch_url = fields.Char(string="Fetch Url")

    backup_file_id = fields.Many2one(
        'ir.attachment',
        string="Backup",
    )

    project_id = fields.Many2one(
        'sass.project',
        string='Project'
    )

    @api.model
    def create(self, vals):
        if 'fetch_url' in vals:
            attachment_obj = self.env['ir.attachment'].with_context(
                force_filestore=True
            )

            with tempfile.NamedTemporaryFile('wb') as fetched_file:
                # Create the attachment with filename because
                # binary data is too large
                datas = b64encode(vals['fetch_url'].encode('utf-8'))
                attachment_data = {
                    "name": "%s.zip" % vals['name'],
                    "datas": datas
                }
                attachment = attachment_obj.create(attachment_data)
                full_path = attachment._full_path(attachment.store_fname)

                # Fetch the file
                req = requests.get(vals['fetch_url'])
                for chunk in req.iter_content(chunk_size=1024):
                    fetched_file.write(chunk)
                fetched_file.seek(0)

                # Copy the tempfile to filestore
                # Eventually we may want a backup filestore
                shutil.copyfile(
                    fetched_file.name,
                    full_path
                )

            vals['backup_file_id'] = attachment.id

        return super(SassProjectDatabase, self).create(vals)

    @api.model
    def _create_db(
        self,
        env,
        name,
    ):
        conn = psycopg2.connect(
            dbname='postgres',
            user=env['PGUSER'],
            password=env['PGPASSWORD'],
            host=env['PGHOST'],
            port=env['PGPORT']
        )
        conn.autocommit = True

        cr = conn.cursor()

        cr.execute(
            "SELECT datname FROM pg_database WHERE datname = %s",
            (name,),
        )

        if cr.fetchall():
            raise DatabaseExists("database %r already exists!" % (name,))
        else:
            chosen_template = odoo.tools.config['db_template']
            collate = "LC_COLLATE 'C'" if chosen_template == 'template0' else ""
            cr.execute(
                """CREATE DATABASE "%s" ENCODING 'unicode' %s TEMPLATE "%s" """ %
                (name, collate, chosen_template)
            )

        cr.close()
        conn.close()

    #@api.model
    #def _reset_db(
    #    self,
    #    env,
    #    name
    #):
    #    commands = [
    #        "delete from fetchmail_server;",
    #        "delete from ir_mail_server;",
    #        "delete from ir_config_parameter where key like 'database.%';"
    #   ]

    #   for cmd in commands:
    #       args = [
    #           '-q', '-d', name, '-c', cmd
    #       ]
    #       exec_pg_command(
    #           env,
    #           'psql',
    #           args,
    #           ignore_error=True
    #       )

    @api.model
    def _restore_db(
        self,
        env,
        name,
        attachment,
    ):
        full_name = attachment._full_path(attachment.store_fname)

        zip_dump = zipfile.ZipFile(full_name, 'r')

        with odoo.tools.osutil.tempdir() as dump_dir:
            filenames = [
                name for name in zip_dump.namelist()
                if '/' not in name
                if 'sql' in name
            ]

            sql_dump = filenames[0]

            zip_dump.extractall(dump_dir, [sql_dump])

            dump_file_path = path.join(dump_dir, 'dump.sql')
            
            if sql_dump.endswith('gz'):
                gzip_dump = path.join(dump_dir, sql_dump)
                with gzip.open(gzip_dump, 'rb') as fin:
                    with open(dump_file_path, 'wb') as fout:
                        shutil.copyfileobj(fin, fout)

            cmd = 'psql'
            args = []

            args.append('-q')

            args.append('-d')
            args.append(name)

            args.append('-f')
            args.append(dump_file_path)

            exec_pg_command(env, cmd, args)

    def _reset_db(
        self,
        env,
        name
    ):
        commands = [
            "delete from fetchmail_server;",
            "delete from ir_mail_server;",
            "delete from ir_config_parameter WHERE key='database.create_date';",
            "delete from ir_config_parameter WHERE key='database.enterprise_code';",
            "delete from ir_config_parameter WHERE key='database.expiration_date';",
            "delete from ir_config_parameter WHERE key='database.expiration_reason';",
            "delete from ir_config_parameter where key='database.uuid';",
            "delete from ir_config_parameter where key='database.secret';",
            "insert into ir_config_parameter (key, value) values ('database.uuid', '%s');" % (uuid1()),
            "insert into ir_config_parameter (key, value) values ('database.secret', '%s');" % (uuid1()),
        ]

        for command in commands:
            exec_pg_query(env, name, command)

    @api.model
    def _drop_db(
        self, 
        env,
        name,
    ):
        cmd = 'psql'
        args = [
            '-q',
            '-d', 'postgres',
            '-c',
            'drop database "%s";' % name
        ]
        exec_pg_command(env, cmd, args, ignore_error=True)

    @api.multi
    def restore_backup(
        self,
        name,
        user='postgres',
        password=None,
        host='127.0.0.1',
        port=5432
    ):
        self.ensure_one()

        env = exec_pg_environ(
            user, password, host, port
        )
        self._drop_db(env, name)
        self._create_db(env, name)
        self._restore_db(
            env,
            name,
            self.backup_file_id,
        )
        self._reset_db(env, name)
        # if re.match('\d+_.*', name) is not None:
        #     self._reset_db(env, name)

    @api.model
    def drop_db(
        self,
        name,
        user='postgres',
        password=None,
        host='127.0.0.1',
        port=5432
    ):
        env = exec_pg_environ(
            user, password, host, port
        )
        self._drop_db(env, name)

    @api.multi
    def create_db(
        self,
        name,
        user='postgres',
        password=None,
        host='127.0.0.1',
        port=5432
    ):
        self.ensure_one()
        env = exec_pg_environ(
            user, password, host, port
        )
        self._create_db(env, name)



class SassProject(models.Model):
    _inherit = 'sass.project'

    database_ids = fields.One2many(
        'sass.project.database',
        'project_id',
        string="DB dumps"
    )

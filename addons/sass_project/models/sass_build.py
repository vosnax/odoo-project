# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
import os
from os import path
import docker
from tempfile import TemporaryDirectory
from datetime import datetime, timedelta
import toml
import logging
from odoo.addons.queue_job.job import job

log = logging.getLogger(__name__)



class SassBuild(models.Model):

    _inherit = 'sass.build'

    name = fields.Char(
        string="Name"
    )

    parent_id = fields.Many2one(
        "sass.build",
        string="Parent Build"
    )

    children_ids = fields.One2many(
        "sass.build",
        "parent_id",
        string="Children Builds"
    )

    starred = fields.Boolean(
        string="Starred",
    )

    expiry_date = fields.Datetime(
        compute="compute_expiry_date",
        store=True,
        string="Expiry Date",
    )

    deploy_date = fields.Datetime(
        string="Deploy Date"
    )

    service_id = fields.Many2one(
        "docker.service",
        string="Service",
    )

    spec_config = fields.Text(
        string="Spect Config"
    )

    state = fields.Selection(
        selection=[
            ('new', _('New')),
            ('preparing', _('Preparing')),
            ('done', _('Done')),
            ('failed', _('Failed'))
        ],
        string="State",
        default="new"
    )

    @api.multi
    def get_all_children(self):
        builds = self.browse([])

        if len(self) == 0:
            return builds

        for obj in self:
            builds += obj.children_ids

        return builds + builds.get_all_children()
            

    @api.depends("starred", "deploy_date")
    def compute_expiry_date(self):
        for obj in self:
            if obj.starred:
                obj.expiry_date = datetime.now() + timedelta(days=365)
            elif obj.deploy_date:
                obj.expiry_date = obj.deploy_date + timedelta(days=7)
            else:
                obj.expiry_date = False

    @job
    def deploy(self, update=False):
        template = self.project_id.template_id

        self.state = 'preparing'
        self.env.cr.commit()

        if update:
            service = self.get_service_config()
            for addon in service.get('addons', []):
                if 'branch' in addon:
                    del addon['branch']
            self.set_service_config(service)

        service_data = template._prepare_service_data(
            self.project_id, self
        )

        if self.service_id:
            if 'name' in service_data:
                del service_data['name']

            self.service_id.write(service_data)
        else:
            service_obj = self.env['docker.service']
            service_id = service_obj.create(service_data)
            self.service_id = service_id.id

        service = self.get_service_config()
        service_labels = []
        for label in self.service_id.label_ids:
            service_labels.append({
                "key": label.key,
                "value": label.value
            })
        service['labels'] = service_labels
        service['image'] = self.service_id.image_id.tag
        service['networks'] = [
            network.name
            for network in self.service_id.network_ids
        ]
        service_mounts = []
        for mount in self.project_id.template_id.mount_template_ids:
            service_mounts.append({
                "type": mount.type,
                "src": mount.source,
                "dst": mount.target,
                "opt": mount.read_access
            })
        service['mounts'] = service_mounts
        service['odoo_version'] = self.project_id.odoo_version
        service['default_branch'] = self.project_id.default_branch
        service['scale'] = self.service_id.replicas
        service['force'] = True
        service['type'] = 'odoo'
        service['hostname'] = self.service_id.hostname
        service['name'] = self.service_id.name
        self.set_service_config(service)

        self.state = 'done'

    def get_service_config(self):
        services = toml.loads(self.spec_config or '[[services]]')
        service = (
            services['services'][0]
            if 'services' in services and len(services['services']) > 0
            else {}
        )
        return service

    @job
    def remove(self):
        for build in self:
            children = build.get_all_children()
            for child in children:
                child.unlink()
            if build.service_id:
                build.service_id.unlink()
            build.unlink()

    def set_service_config(self, service):
        self.spec_config = toml.dumps({'services': [service]})

    def get_custom_ref(self, url, default_branch):
        return default_branch

# -*- coding: utf-8 -*-
from odoo import api, models, fields, _

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend


class GitProjectSpec(models.Model):
    _inherit = 'sass.project.spec'
    _rec_name = 'url'

    url = fields.Char(string="URL")
    ref = fields.Char(string="Ref", help="Commit, Branch or Tag")
    subfolder = fields.Char(string="Subfolder")

    private_key = fields.Text(
        related="key_id.private_key",
        string="Private Key"
    )
    public_key = fields.Text(
        related="key_id.public_key",
        string="Public Key"
    )
    key_id = fields.Many2one(
        'ssh.key',
        string='SSH Key'
    )

    sass_project_id = fields.Many2one(
        'sass.project',
        string="Sass Project"
    )

    @api.model
    def create(self, vals):
        res = super(GitProjectSpec, self).create(vals)

        if not res.key_id:
            key = res.key_id.create({}) 
            res.key_id = key.id

        return res


class DockerMount(models.Model):
    _inherit = 'docker.mount'

    # TODO bind it to a real git project instead
    project_id = fields.Many2one(
        'sass.project.spec',
        string="Sass Project Spec",
    )

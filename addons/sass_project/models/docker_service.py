# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class DockerService(models.Model):

    _inherit = 'docker.service'

    sass_project_id = fields.Many2one(
        'sass.project',
        string="Sass Project"
    )

    @api.multi
    def get_db_name(self):
        return self.hostname

    @api.multi
    def restore_backup(self):
        self.ensure_one()

        project = self.sass_project_id
        base_path = '.%s' % project.base_domain
        branch_name = self.name.replace(base_path, '')

        db_name = self.get_db_name()

        database = self.sass_project_id.database_ids[-1]

        env = {}
        for line in self.environment_variables.split('\n'):
            parts = line.split('=', 1)
            if len(parts) == 2:
                env[parts[0]] = parts[1]

        database.restore_backup(
            db_name,
            user=env['USER'],
            password=env['PASSWORD'],
            host=env['HOST'],
            port=5432
        )

    @api.multi
    def drop_db(self):
        self.ensure_one()

        project = self.sass_project_id
        base_path = '.%s' % project.base_domain
        branch_name = self.name.replace(base_path, '')
        db_name = self.get_db_name()

        env = {}
        for line in self.environment_variables.split('\n'):
            parts = line.split('=', 1)
            if len(parts) == 2:
                env[parts[0]] = parts[1]

        self.env['sass.project.database'].drop_db(
            db_name,
            user=env['USER'],
            password=env['PASSWORD'],
            host=env['HOST'],
            port=5432
        )

    @api.multi
    def create_db(self):
        self.ensure_one()

        project = self.sass_project_id
        base_path = '.%s' % project.base_domain
        base_path = base_path.replace('.', '_')
        branch_name = self.name.replace(base_path, '')
        db_name = self.get_db_name()

        database = self.sass_project_id.database_ids[-1]

        env = self.get_environment_variables_dict()

        database.create_db(
            db_name,
            user=env['USER'],
            password=env['PASSWORD'],
            host=env['HOST'],
            port=5432
        )

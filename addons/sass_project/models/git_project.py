# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class GitProject(models.Model):

    _inherit = 'git.project'

    sass_project_id = fields.Many2one(
        'sass.project',
        string="Sass Project"
    )

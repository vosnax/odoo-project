# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class PythonPackages(models.Model):
    _name = 'sass.packages.python'

    name = fields.Char(string="Name")

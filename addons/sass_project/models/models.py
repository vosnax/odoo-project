# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class SassProject(models.Model):
    _name = 'sass.project'
    _description = "Sass Project"


class GitProjectSpec(models.Model):
    _name = 'sass.project.spec'
    _description = "Sass Project Spec"


class SassServiceTemplate(models.Model):
    _name = 'sass.service.template'
    _description = "Sass Service Template"


class MountTemplate(models.Model):
    _name = 'sass.mount.template'
    _description = 'Sass Mount Template'


class SassBuild(models.Model):
    _name = 'sass.build'
    _description = "Sass build"

class SSHKey(models.Model):
    _name = 'ssh.key'
    _description = 'SSH Key'

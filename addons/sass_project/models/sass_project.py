# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
import os
from random import random
from os import path
import docker
from tempfile import TemporaryDirectory

import logging
import toml

log = logging.getLogger(__name__)

NULL_COMMIT = '0000000000000000000000000000000000000000'

class SassBuild(models.Model):
    _inherit = 'sass.build'

    project_id = fields.Many2one(
        "sass.project",
        string="Project"
    )

class SassProject(models.Model):

    _inherit = 'sass.project'

    name = fields.Char(
        string="Name",
        required=True
    )

    base_domain = fields.Char(
        string="Base domain"
    )

    image = fields.Binary(
        "Variant Image",
        attachment=True,
        help="This field holds the image used as image for the product variant, limited to 1024x1024px.",
    )

    partner_id = fields.Many2one(
        'res.partner',
        string="Partner",
    )

    # TODO try to use a many2many instead.
    # Project could be used in multiple sass
    # projects or even not linked directly
    git_project_ids = fields.One2many(
        'git.project',
        'sass_project_id',
        string="Git Project"
    )

    git_projects = fields.One2many(
        'sass.project.spec',
        'sass_project_id',
        string="Extra projects",
    )

    debian_packages = fields.Many2many(
        'sass.packages.debian',
        string="Debian Packages"
    )

    python_packages = fields.Many2many(
        'sass.packages.python',
        string="Python Packages"
    )

    service_ids = fields.One2many(
        'docker.service',
        'sass_project_id',
        string="Services"
    )

    default_branch = fields.Char(
        string="Default Branch"
    )

    base_image = fields.Many2one(
        "docker.image",
        string="Base Image"
    )

    odoo_version = fields.Selection(
        selection=[
            ('8.0', '8.0'),
            ('9.0', '9.0'),
            ('10.0', '10.0'),
            ('11.0', '11.0'),
            ('12.0', '12.0'),
            ('13.0', '13.0'),
            ('14.0', '14.0'),
        ],
        string="Version",
        default="13.0"
    )

    template_id = fields.Many2one(
        'sass.service.template',
        string="Template",
        help="Template used to setup the project and depencencies"
    )

    build_ids = fields.One2many(
        "sass.build",
        "project_id",
        string="Builds"
    )

    root_build_ids = fields.One2many(
        "sass.build",
        "project_id",
        domain=[['parent_id', '=', False]],
        string="Root Builds"
    )

    spec_config = fields.Text(
        string="Spect Config"
    )

    @api.multi
    def create_build(self, name, spec):
        build_obj = self.env['sass.build']

        build = build_obj.create({
            "name": name,
            "project_id": self.id,
            "spec_config": toml.dumps(spec)
        })

        return build

    # TODO move to a queue
    # DEPRECATED
    @api.multi
    def plan_deployment_old(self, push):
        source_branch = push.ref.replace('refs/heads/', '')

        service_obj = self.env['docker.service']

        keep = False
        base_domain = self.base_domain

        if source_branch == self.main_branch:
            keep = True
            hostname = "%s.%s" % (self.name, base_domain)
        else:
            hostname = "%s_%s.%s" % (
                source_branch,
                self.name,
                base_domain
            )

        # hostname = hostname.replace('.', '_')

        service = service_obj.search([['hostname', '=', hostname]], limit=1)

        # Remove service when branch is removed
        if push.after_commit_id == NULL_COMMIT and service:
            service.unlink()
            return

        # Create a new image regardless of the service existing or not
        image = self.build_new_image(push, hostname)

        if service and not keep:
            service.unlink()
            service = None

        # Update the image on the service or create a new one
        if not service:
            self_ctx = self.with_context(new_service=True)
            service = self_ctx.template_id.create_service(
                self,
                hostname,
                image,
                push
            )
            self_ctx.service_ids += service
        else:
            self.template_id.update_mount(
                self,
                hostname,
                image,
                push,
                service
            )
            service.image_id = image

    # DEPRECATED
    @api.multi
    def build_new_image(self, push, hostname):
        self.ensure_one()
        client = docker.from_env()
        image_obj = self.env['docker.image']

        with TemporaryDirectory() as dirpath:
            self.prepare_build(dirpath, hostname)

            image_name = "%s-%s:%s" % (
                self.name,
                push.ref.replace('refs/heads/', ''),
                push.after_commit_id
            )

            image, stream = client.images.build(
                tag=image_name,
                path=dirpath
            )

            oimage = image_obj.create({
                "docker_id": image.id.split(':')[1],
                "tag": image.tags[0]
            })

            return oimage

    # DEPRECATED
    @api.multi
    def compute_docker_file(self, extra_addons, hostname):
        """
        Compute the docker file
        TODO: replace this code using a template of some sort
        for example mako templating could be used here
        """

        odoo_version = float(self.odoo_version.split(':', 1)[1])
        if odoo_version > 10:
            pip='pip3'
            python='python3'
        else:
            pip='pip'
            python='python2.7'

        def to_path(dirname):
            return '/addons/%s' % dirname

        template_lines = [
            "FROM %(image_from)s",
            "USER root",
            "RUN mkdir -p /var/lib/apt/lists/partial",
            "RUN apt-get clean",
            "RUN apt-get update",
            "RUN apt-get install -y python-pip",
        ]

        if self.mapped('debian_packages.name'):
            template_lines += [
                "RUN apt-get install -y %(debian_packages)s",
            ]
        if self.mapped('python_packages.name'):
            template_lines += [
                "RUN %(pip)s install %(python_packages)s",
            ]

        template_lines += [
            (
                'RUN sed -i -e '
                '"s/%(rule)s.*/%(rule)s %(addons)s/g" '
                '%(config_file)s'
            ),
            (
                'RUN sed -i -e '
                '"s/; dbfilter.*/dbfilter = %(dbfilter)s/g" '
                '%(config_file)s'
            ),
            (
                'RUN printf "list_db = False\\n" >> /etc/odoo/odoo.conf'
            ),
            (
                'RUN printf "proxy_mode = True\\n" >> /etc/odoo/odoo.conf'
            ),
            "USER odoo",
            "ENTRYPOINT [\"/entrypoint.sh\"]",
            "CMD [\"odoo\"]\n"
        ]

        base_template = "\n".join(template_lines)

        debian_packages = " ".join(self.mapped('debian_packages.name'))
        python_packages = " ".join(self.mapped('python_packages.name'))

        addons = ",".join(map(to_path, extra_addons))
        addons = "/usr/lib/%(python)s/dist-packages/odoo/addons,%(addons)s" % {
            "addons": addons,
            "python": python
        }
        addons = addons.replace('/', '\/')

        return base_template % {
            'config_file': '/etc/odoo/odoo.conf',
            'rule': 'addons_path =',
            'addons': addons,
            'image_from': self.odoo_version,
            'python_packages': python_packages,
            'debian_packages': debian_packages,
            'dbfilter': '^%d.*$',
            'pip': pip
        }

    # DEPRECATED
    @api.multi
    def prepare_build(self, directory, hostname):
        self.ensure_one()

        filename = path.join(
            directory, 'Dockerfile'
        )

        extra_addons = []

        for project in self.git_projects:
            project_name = path.basename(project.url).replace('.git', '')
            extra_addons.append("%s-%s" % (project_name, project.ref))

        dockerfile_txt = self.compute_docker_file(extra_addons, hostname)

        log.info(dockerfile_txt)

        with open(filename, 'w') as dockerfile:
            dockerfile.write(dockerfile_txt)

    def get_root_builds(self):
        return self.root_build_ids 

    def get_default_name(self):
        def char_range(from_c, to_c):
            return [
                chr(let)
                for let in range(ord(from_c), ord(to_c) + 1)
            ]
        letters = char_range('a', 'z')
        nums = char_range('0', '9')
        symbols = letters + nums

        def random_pick(symbols, count):
            for idx in range(count):
                index = random() * len(symbols)
                yield symbols[int(index)]

        subdomain = "".join([x for x in random_pick(symbols, 10)])

        return "%s.%s" % (subdomain, self.base_domain)

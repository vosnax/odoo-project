# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
from odoo.exceptions import Warning
import os
from os import path
import tempfile
from tempfile import TemporaryDirectory
from subprocess import call as run
import logging
import docker
from docker.errors import ImageNotFound
import shutil
from giturlparse import parse
import toml


log = logging.getLogger(__name__)


def slug(txt):
    new_url = parse(txt)
    url_format = "%(domain)s-%(owner)s_%(repo)s"
    ret = url_format % new_url.data
    return ret


class ServiceLabel(models.Model):
    _inherit = 'docker.label'

    template_id = fields.Many2one(
        'sass.service.template',
        string="Template",
        copy=False,
    )


class DockerEnv(models.Model):
    _inherit = 'docker.env'

    template_id = fields.Many2one(
        'sass.service.template',
        string="Template",
        copy=False,
    )


class SassServiceTemplate(models.Model):
    _inherit = 'sass.service.template'

    name = fields.Char(string="Name")
    image_id = fields.Many2one(
        "docker.image",
        string="Image"
    )
    args = fields.Char(string="Args")
    mount_template_ids = fields.One2many(
        'sass.mount.template',
        'template_id',
        string='Mounts',
        copy=True
    )

    network_ids = fields.Many2many(
        'docker.network',
        string="Networks"
    )

    env_ids = fields.One2many(
        "docker.env",
        "template_id",
        string="Environment Variables",
        copy=True
    )

    label_ids = fields.One2many(
        'docker.label',
        'template_id',
        string="Labels",
        copy=True,
    )

    volume_driver_id = fields.Many2one(
        'docker.driver',
        string="Volume Driver",
        domain=[['type', '=', 'volume']],
    )

    def _prepare_dict_data(self, project, build):
        dict_data = {
            "hostname": build.name,
            "project_name": project.name.replace(' ', '_'),
            "build_name": build.name.replace('.', '_'),
        }

        return dict_data

    def _prepare_labels(self, dict_data):
        return [
            [0, None, {
                "key": label.key % dict_data,
                "value": label.value % dict_data
            }]
            for label in self.label_ids
        ]

    def _prepare_environment_variables(self, build, dict_data):
        env_obj = self.env['docker.env']
        env_ids = env_obj.browse([])

        for env in self.env_ids:
            env_ids += env.copy({
                "value": env.value % dict_data,
                "template_id": False,
            })

        # Load env from current service build object
        service = build.get_service_config()
        for env in service.setdefault('env', []):
            env_ids += env_obj.create({
                "key": env['key'],
                "value": env['value'] % dict_data
            })

        return env_ids

    def _update_spec(self, build, data):
        # Complete service spec from what we can directly parse
        service = build.get_service_config()
        for label in data.get('label_ids'):
            service['labels'].append(label[2])

    def _prepare_service_data(self, project, build):
        mount_ids = self._create_mounts(project, build)
        dict_data = self._prepare_dict_data(project, build)
        env_ids = self._prepare_environment_variables(build, dict_data)
        label_ids = self._prepare_labels(dict_data)

        build_name = "%s_%s" % (
            build.name.replace('.', '_'),
            build.id
        )

        service_data = {
            'name': build_name,
            'hostname': build.name,
            'image_id': self.image_id.id,
            'mount_ids': [[6, None, mount_ids.ids]],
            'network_ids': [[6, None, self.network_ids.ids]],
            'env_ids': [[6, None, env_ids.ids]],
            'label_ids': [[5]] + label_ids,
        }

        if self.args:
            service_data['args'] = self.args

        return service_data

    # Deprecated
    @api.multi
    def create_service(self, project, hostname, image, push):
        """
        Create the service using a template
        """
        service_obj = self.env['docker.service']
        service_data = self._prepare_service_data(
            project, hostname, image, push
        )
        service = service_obj.create(service_data)
        return service

    # Deprecated
    @api.multi
    def update_mount(self, project, hostname, image, push, service):
        push_context = self.with_context(
            GIT_URL=push.project_id.ssh_url,
            GIT_BRANCH=push.ref.replace('refs/heads/', ''),
            GIT_COMMIT=push.after_commit_id
        )

        for git_project in project.git_projects:
            if git_project.url == push.project_id.ssh_url:
                break

        old_mount_ids = service.mount_ids

        for to_remove in old_mount_ids:
            if to_remove.project_id.id == git_project.id:
                break

        mount_ids = push_context.get_addons_mount(
            project, git_project, hostname
        )
        new_mount_ids = old_mount_ids + mount_ids - to_remove

        service.write({
            "mount_ids": [[6, None, new_mount_ids.ids]]
        })

        to_remove.unlink()

    def _create_mounts(self, project, build):
        """
        Create the mounting point based on the
        templates.
        """
        mount_obj = self.env['docker.mount']
        mounts = mount_obj.browse([])

        mounts += self.get_template_mounts(project, build)
        mounts += self.get_addons_mount(project, build)

        return mounts

    @api.multi
    def get_template_mounts(self, project, build):
        mount_obj = self.env['docker.mount']
        volume_obj = self.env['docker.volume']
        mounts = mount_obj.browse([])

        for template in self.mount_template_ids:

            data = {}
            data['type'] = template.type
            data['target'] = template.target
            data['read_access'] = template.read_access

            if template.type == 'bind':
                data['source_txt'] = template.source_txt
            elif template.type == 'volume':
                volume_name = "%s.%s" % (
                    template.volume_name,
                    build.name.replace('.', '_')
                )

                volume = volume_obj.search(
                    [['name', '=', volume_name]], limit=1
                )

                if not volume:
                    volume = volume_obj.create({
                        "name": volume_name,
                        "driver_id": template.driver_id.id,
                    })

                data['volume_id'] = volume.id

            mount = mount_obj.create(data)
            mounts += mount

        return mounts

    @api.multi
    def get_addons_mount(self, project, build):
        mount_obj = self.env['docker.mount']
        mounts = mount_obj.browse([])

        service = build.get_service_config()

        addons_branches = {}
        for addon in service.setdefault('addons', []):
            if 'url' in addon and 'branch' in addon and addon['branch']:
                addons_branches[parse(addon['url']).url2https] = addon['branch']

        for git_project in project.git_projects:
            url = parse(git_project.url).url2https
            project_name, ref, volume = self.fetch_addons_volume(
                git_project,
                build,
                addons_branches.get(url)
            )
            target = "%s-%s" % (project_name, ref)

            data = {}
            data['type'] = 'volume'
            data['target'] = '/addons/%s' % target
            data['read_access'] = 'ro'
            data['volume_id'] = volume.id
            data['project_id'] = git_project.id

            mount = mount_obj.create(data)
            mounts += mount

            ref = build.get_custom_ref(parse(url).url2https, ref)

            for addon in service['addons']:
                https_url = parse(addon.get('url')).url2https
                if https_url == url:
                    addon['branch'] = ref
                    break
            else:
                service['addons'].append({
                    "url": parse(url).url2https,
                    "branch": ref
                })

        # Store back what we just deployed. It will be useful for
        # rolling back to a previous build
        build.set_service_config(service)

        return mounts

    @api.multi
    def _get_clone_or_fetch_commands(self, project, build, new_volume):
        """
        Get the command to run to fetch/update global repo.

        Override this method to add token for example for gitlab
        or any other possible provider.
        """
        #volume_obj = self.env['docker.volume']
        #project_url = project.url
        #project_volume_name = slug(project_url)
        #proj_vol = volume_obj.search(
        #    [['name', '=', project_volume_name]], limit=1
        #)
        #client = docker.from_env()
        #try:
        #    client.volumes.get(project_volume_name)
        #except Exception:
        #    if proj_vol:
        #        proj_vol.unlink()
        #    proj_vol = None
        
        log.info('Selecting command for project %s' % (project.url))

        if new_volume:
            clone_fetch = "git clone %(url)s %(folder)s"
        else:
            clone_fetch = "cd %(folder)s && git fetch --all"

        return [clone_fetch]

    @api.multi
    def fetch_addons_volume(self, project, build, checkout=None):
        """
        Fetch code into volumes

        In practice, we really dont care about if the volume
        exist and which name to give. As long as it's passed to
        the mount above.

        We also don't really care about the way we fetch the
        data. We could use gitlab/github apis with the correct
        authenticate to get access to the archives directly for
        a particular commit.

        One key point is that we should know what to fetch a
        possibly not try to update an existing volume. We either
        create or reuse an existing volume but updating a volume
        could cause more problems than it solves.
        Say we have a project called "website".

        The volume fetched should take the form
        wesbite:commit_id with label branch=staging

        It could also be something like

        name = commit_id
        labels
        - project = website
        - branch = staging

        The main difference here is that when changing the
        service spec it will trigger a service update but if
        we update the volume directly we won't be able to see
        any change in the spec and code won't be reloaded.

        One solution to the authentication process would be to
        generate private keys and provide the public keys on
        the UI.

        It's easy to setup for us be tedious to maintain unless
        we also add the public keys to a user with the
        gitlab/github api.

        Start a new container with the ssh key in its home and
        clone or checkout directly in the folder.

        The big advantage here is that with this method any ssh
        git backend is supported out of the box and public keys
        could be handled automatically for git backend that have
        the APIs.

        The main problem is on which account to we setup the
        keys obviously.

        Also, we should always try to fetch a new repo.
        We probably need to have git repository caches and then
        when required we fetch the new commit using this or that
        user keys.
        If a fetch fails with permissions right, we bubble the
        error up. If the fetch success we can checkout
        the right commit in a new volume.

        The ideal procedure would be like this:

        1. Search for a cache git project volume
        2. If project is not cached, fetch it
        3. If project is already cached fetch for more commits
        4. Checkout the ref required and return the commit_id
        5. Create a new volume with the commit id
        6. Copy the code to the new volume for the wanted ref
        7. Return the volume
        """
        volume_obj = self.env['docker.volume']
        client = docker.from_env()

        # project_branch = project.ref
        project_url = project.url
        project_name = path.basename(project.url).replace('.git', '')

        # ENV = os.environ.copy()
        # ENV['HOME'] = '/var/lib/odoo'
        # ENV['GIT_SSH_COMMAND'] = "ssh -i /var/lib/odoo/.ssh/id_rsa"
        # log.info(ENV)

        # Get project cache
        new_volume = False
        project_volume_name = slug(project_url)
        proj_vol = volume_obj.search(
            [['name', '=', project_volume_name]], limit=1
        )
        if not proj_vol:
            new_volume = True
            proj_vol = volume_obj.create({
                "name": project_volume_name,
                "driver_id": self.volume_driver_id,
            })

        volumes = {}
        volumes[proj_vol.name] = {
            "bind": "/cache",
            "mode": "rw"
        }

        ctx = {
            'folder': '/cache/project',
            'url': project.url,
            'branch': (
                checkout or
                project.ref or
                build.project_id.default_branch
            )
        }

        tmpssh = tempfile.mkdtemp()

        log.info('Writing to %s' % tmpssh)

        # TODO reduce access rights
        os.chmod(tmpssh, 0o777)

        # TODO join all possibly ssh keys together
        key = build.project_id.partner_id.key_ids
        if key and len(key) > 0:
            key = key[0]

        if not key:
            key = project.key_id

        with open(path.join(tmpssh, 'id_rsa'), 'w') as pk:
            pk.write(key.private_key)

        with open(path.join(tmpssh, 'id_rsa.pub'), 'w') as ppk:
            ppk.write(key.public_key)

        # Add as a configuration
        shutil.copyfile(
            '/tmp/known_hosts',
            path.join(tmpssh, 'known_hosts')
        )

        log.info('Files in %s: %s' % (tmpssh, ",".join(os.listdir(tmpssh))))

        volumes[tmpssh] = {
            'bind': '/root/.ssh',
            'mode': 'rw'
        }

        # TODO find a better way to transfer ssh keys or start container
        # TODO have a bettwe way to handle known_hosts. We need a manually
        #      managed list automatically deployed to prevent Man in the
        #      middle attack. Git clone will fail otherwise
        # It could be added as a file in the template
        # For debian with git installed
        command = []
        try:
            # check if this image exist for faster setup
            image = 'llacroix/git:latest'
            client.images.get(image)
        except ImageNotFound:
            image = 'ubuntu:bionic'
            # command required to install git
            command += [
                'apt-get update',
                'apt-get install -y git',
            ]

        # prepare sshaccess
        command += [
            'chown root:root ~/.ssh/id_rsa',
            'chown root:root ~/.ssh/id_rsa.pub',
            'chmod 600 ~/.ssh/id_rsa',
            'chmod 644 ~/.ssh/id_rsa.pub',
            'chown root:root ~/.ssh/known_hosts',
            'chmod 644 ~/.ssh/known_hosts',
            'eval `ssh-agent -s`',
            'ssh-add ~/.ssh/id_rsa',
        ]

        # get the command line to execute to fetch or clone a project
        command += self._get_clone_or_fetch_commands(project, build, new_volume)

        # checkout the branch/commit
        command += [
            'cd %(folder)s && git checkout %(branch)s',
            'cd %(folder)s && git reset --hard origin/%(branch)s',
            'cd %(folder)s && git log -n 1 --pretty=%%H',
        ]


        res = client.containers.run(
            image,
            [
                '/bin/bash',
                '-c',
                ';'.join(command) % ctx
            ],
            environment={
                "HOME": "/root"
            },
            volumes=volumes
        )

        # get last commit of the fetched branch
        del volumes[tmpssh]
        log.info(res)
        commit_id = res.decode().strip().split('\n')[-1]

        log.info(res)

        # convert the volume/commit name as universal one
        volume_name = "%s-%s" % (slug(project_url), commit_id)
        volume = volume_obj.search(
            [['name', '=', volume_name]], limit=1
        )

        # if no checkout exists then we make a checkout volume
        if not volume:
            volume = volume_obj.create({
                "name": volume_name,
                "driver_id": self.volume_driver_id,
            })
            volumes[volume.name] = {
                'bind': '/repo',
                'mode': 'rw'

            }
            src_path = "/cache/project"

            if project.subfolder:
                src_path = path.join(src_path, project.subfolder)

            command = []
            try:
                # check if this image exist for faster setup
                image = 'llacroix/git:latest'
                client.images.get(image)
            except ImageNotFound:
                image = 'ubuntu:bionic'
                # command required to install git
                command += [
                    'apt-get update',
                    'apt-get install -y git',
                ]

            command += [
                'cd /cache/project && git --work-tree=/repo checkout %s -- .' % commit_id
            ]
            # checkout the commit again to be certain no other project
            # did checkout in a different place
            client.containers.run(
                image,
                [
                    '/bin/bash',
                    '-c',
                    ";".join(command)
                ],
                environment={
                    "HOME": "/root"
                },
                volumes=volumes
            )

        return project_name, commit_id, volume

# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class SassProject(models.Model):

    _name = 'sass.project'

    name = fields.Char(
        string="Name",
        required=True
    )

    email = fields.Char(
        string="Email",
        required=True
    )

    partner_name = fields.Char(
        string="Partner name",
        required=True
    )

    partner_id = fields.Many2one(
        'res.partner',
        string="Partner",
    )

    git_projects = fields.Text(
        string="Extra projects",
    )


class OdooDocker(models.Model):

    _inherit = 'docker.container.odoo'

    sass_project_id = fields.Many2one(
        'sass.project',
        string="Sass Project"
    )


class SassProject2(models.Model):

    _inherit = 'sass.project'

    container_ids = fields.One2many(
        'docker.container.odoo',
        'sass_project_id',
        string="Containers"
    )

    @api.model
    def create(self, vals):
        container_obj = self.env['docker.container']
        network_obj = self.env['docker.network']
        odoo_container_obj = self.env['docker.container.odoo']
        image_obj = self.env['docker.image.odoo']
        partner_obj = self.env['res.partner']
        base_image_obj = self.env['docker.image']

        res = super(SassProject2, self).create(vals)

        hostname = "%s.o.archeti.ca" % res.name

        partner = partner_obj.create({
            "name": res.partner_name,
            "email": res.email,
        })
        network_id = network_obj.search([['name', '=', 'odoonet']], limit=1)

        if not res.git_projects:
            image_id = image_obj.search([['name', '=', 'full']], limit=1)
        else:
            real_image_id = base_image_obj.create({"name": res.name})
            deps = res.git_projects.split('\n')
            deps = "\n".join(map(lambda x: x.strip(), deps))
            image_id = image_obj.create({
                "image_id": real_image_id.id,
                "python_deps": "simplejson",
                "odoo_projects": deps,
                "image_from": "odoo:11.0",
                "version": "11.0",
            })
            image_id.build()

        container = container_obj.create({
            "name": res.name,
            "image_id": image_id.image_id.id,
            "network_id": network_id.id,
        })

        odoo_container_obj.create({
            "container_id": container.id,
            "hostname": hostname,
            "odoo_image_id": image_id.id,
            "sass_project_id": res.id,
        })

        res.partner_id = partner.id

        return res

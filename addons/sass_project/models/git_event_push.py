# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class GitEventPush(models.Model):
    _inherit = 'git.event.push'

    def handle_request(self, backend, data):
        """
        Create/Update a service using a git event

        TODO handle deleted branches
        """
        res = super(GitEventPush, self).handle_request(backend, data)

        project_id = res.project_id
        sass_project = project_id.sass_project_id

        #if sass_project:
        #    sass_project.plan_deployment(res)

        return res

    @api.multi
    def plan_deployment(self):
        self.ensure_one()
        project_id = self.project_id
        sass_project = project_id.sass_project_id

        #if sass_project:
        #    sass_project.plan_deployment(self)

        return True

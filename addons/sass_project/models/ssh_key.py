# -*- coding: utf-8 -*-
from odoo import api, models, fields, _

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend


class SSHKey(models.Model):
    _inherit = 'ssh.key'

    name = fields.Char(string="Name")
    private_key = fields.Text(string="Private Key")
    public_key = fields.Text(string="Public Key")

    @api.model
    def create(self, vals):
        res = super(SSHKey, self).create(vals)

        if not res.private_key:
            res.setup_keys()

        return res

    @api.multi
    def write(self, vals):
        res = super(SSHKey, self).write(vals)

        for obj in self:
            if not obj.private_key:
                obj.setup_keys()

        return res

    @api.multi
    def setup_keys(self):
        for obj in self:
            key = rsa.generate_private_key(
                backend=default_backend(),
                public_exponent=65537,
                key_size=2048
            )

            private_key = key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.PKCS8,
                encryption_algorithm=serialization.NoEncryption()
            ).decode()

            public_key = key.public_key()

            public_key_encoded = public_key.public_bytes(
                encoding=serialization.Encoding.OpenSSH,
                format=serialization.PublicFormat.OpenSSH
            )

            public_key_encoded = "%s %s\n" % (
                public_key_encoded.decode(), obj.name
            )

            obj.write({
                "private_key": private_key,
                "public_key": public_key_encoded,
            })

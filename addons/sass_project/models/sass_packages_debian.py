# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class DebianPackages(models.Model):
    _name = 'sass.packages.debian'

    name = fields.Char(string="Name")

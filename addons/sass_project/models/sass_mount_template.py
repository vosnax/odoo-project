# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class MountTemplate(models.Model):
    _inherit = 'sass.mount.template'

    type = fields.Selection(
        selection=[
            ('bind', _('Bind')),
            ('volume', _('Volume'))
        ],
        string="Type"
    )

    name = fields.Char(compute="_compute_name", string="Name")
    source = fields.Char(compute='_compute_source', string="Source")
    source_txt = fields.Char(string="Source Text")
    volume_name = fields.Char(string="Volume Name")
    driver_id = fields.Many2one(
        "docker.driver",
        domain=[['type', '=', 'volume']],
        string="Driver"
    )
    target = fields.Char(string="Target")
    read_access = fields.Selection(
        selection=[
            ('rw', _('Read/Write')),
            ('ro', _('Read only'))
        ],
        string="Access",
        default="rw"
    )
    template_id = fields.Many2one(
        'sass.service.template',
        string="Service Template"
    )

    @api.depends('volume_name', 'source_txt', 'type')
    def _compute_source(self):
        for obj in self:
            if obj.type == 'bind':
                obj.source = obj.source_txt
            elif obj.type == 'volume':
                obj.source = obj.volume_name

    @api.depends('source', 'target', 'read_access')
    def _compute_name(self):
        for obj in self:
            obj.name = '%s:%s:%s' % (
                obj.source, obj.target, obj.read_access
            )

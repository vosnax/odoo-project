# -*- coding: utf-8 -*-
from odoo import api, models, fields, _

class SSHKey(models.Model):
    _inherit = "ssh.key"

    partner_id = fields.Many2one(
        'res.partner',
        string="Partner"
    )

class ResPartner(models.Model):
    _inherit = 'res.partner'

    key_ids = fields.One2many(
        'ssh.key',
        'partner_id',
        string="SSH Keys"
    )

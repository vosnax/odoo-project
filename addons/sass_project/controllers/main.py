# -*- coding: utf-8 -*-
import json
import logging
from collections import defaultdict
from werkzeug.exceptions import Forbidden, NotFound

from odoo import fields, http, tools, _
from odoo.http import request
from odoo.addons.base.models.ir_qweb_fields import nl2br
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.website.controllers.main import QueryURL
from odoo.exceptions import ValidationError
from odoo.addons.website.controllers.main import Website
from odoo.addons.website_form.controllers.main import WebsiteForm
from odoo.osv import expression
import docker

from odoo.http import Response
import os
import sys
import time
import threading
import signal

class RestartServer(threading.Thread):
    def run(self):
        time.sleep(2)
        pgid = os.getpgid(0)
        os.killpg(pgid, signal.SIGTERM)
        #sys.exit(0)


class ProjectController(http.Controller):

    @http.route(
        [
            '/sass_projects',
        ],
        type="http",
        auth="user",
        methods=["POST", "GET"],
        website=True
    )
    def get_projects(self, search='', **kwargs):
        project_obj = request.env['sass.project'].sudo()

        projects = project_obj.search([])
        keep = QueryURL('/projects', search=search, order=kwargs.get('order'))
        values = {
            "projects": projects,
            "keep": keep,
            "slug": slug,
        }

        return request.render("sass_project.projects", values)


    @http.route(
        [
            '/sass_projects/<model("sass.project"):project>',
        ],
        type="http",
        auth="user",
        methods=["POST", "GET"],
        website=True
    )
    def get_project(self, project, search='', **kwargs):

        keep = QueryURL('/projects', search=search, order=kwargs.get('order'))

        values = {
            "project": project,
            "keep": keep,
            "slug": slug,
        }

        return request.render("sass_project.project_full_layout", values)


    # /sass_projects/build/create
    @http.route(
        [
            '/sass_projects/<model("sass.project"):project>/build'
        ],
        type="http",
        auth="user",
        methods=["POST", "GET"],
        website=True
    )
    def build_project(self, project, search='', **post):
        hostname = post.get('name') or project.get_default_name()
        
        addons = []
        for key, value in post.items():
            if not key.startswith('sass.project.spec'):
                continue

            model, rec_id, attr = key.split('-')
            spec = request.env['sass.project.spec'].browse(int(rec_id))
            addons.append({
                "url": spec.url,
                "branch": value or spec.ref
            })

        envs = []

        if post.get('master_password'):
            envs.append({
                "key": "MASTER_PASSWORD",
                "value": post.get('master_password')
            })

        build = project.create_build(
            hostname,
            {
                'services': [
                    {
                        "addons": addons,
                        "env": envs
                    }
                ]
            }
        )

        build.sudo().with_delay().deploy()

        project_url = "/sass_projects/%s" % slug(project)

        return request.redirect(request.httprequest.referrer or project_url)

    @http.route(
        [
            '/sass_projects/build/<model("sass.build"):build>/rebuild'
        ],
        type="http",
        auth="user",
        methods=["POST", "GET"],
        website=True
    )
    def get_project_rebuild(self, build, search='', **post):
        new_build = build.copy()
        build.parent_id = new_build.id

        new_build.sudo().with_delay().deploy(update=True)

        project_url = "/sass_projects/%s" % slug(new_build.project_id)

        return request.redirect(request.httprequest.referrer or project_url)

    @http.route(
        [
            '/sass_projects/build/<model("sass.build"):build>/rollback'
        ],
        type="http",
        auth="user",
        methods=["POST", "GET"],
        website=True
    )
    def get_project_rollback(self, build, search='', **post):
        new_build = build.copy()
        build.parent_id = new_build.id

        new_build.sudo().with_delay().deploy()

        project_url = "/sass_projects/%s" % slug(new_build.project_id)

        return request.redirect(request.httprequest.referrer or project_url)

    @http.route(
        [
            '/sass_projects/build/<model("sass.build"):build>'
        ],
        type="http",
        auth="user",
        methods=["POST", "GET"],
        website=True
    )
    def get_build(self, build, search='', **post):
        keep = QueryURL(
            '/projects/build/%s' % build.id,
            search=search,
            order=post.get('order')
        )

        values = {
            "build": build,
            "keep": keep,
            "slug": slug,
        }

        return request.render("sass_project.build_layout", values)

    @http.route(
        [
            '/sass_projects/build/<model("sass.build"):build>/logs_view'
        ],
        type="http",
        auth="user",
        methods=["POST", "GET"],
        website=True
    )
    def get_build_log_view(self, build, search='', **post):
        keep = QueryURL(
            '/projects/build/%s' % build.id,
            search=search,
            order=post.get('order')
        )

        values = {
            "build": build,
            "keep": keep,
            "slug": slug,
        }

        return request.render("sass_project.build_log_layout", values)

    @http.route(
        [
            '/sass_projects/build/<model("sass.build"):build>/logs'
        ],
        type="http",
        auth="user",
        methods=["POST", "GET"],
        website=True
    )
    def get_build_logs(self, build, search='', **post):
        if not build.service_id:
            return "Service Not Started"

        try:
            client = docker.from_env()
            service = client.services.get(build.service_id.docker_id)

            return Response(
                service.logs(stdout=True, stderr=True, follow=True),
                direct_passthrough=True,
                headers=[
                    ('Content-Type', 'text/plain')
                ]
            )
        except Exception as exc:
            return "Something went wrong %s" % exc

    @http.route(
        [
            '/sass_projects/build/<model("sass.build"):build>/remove'
        ],
        type="http",
        auth="user",
        methods=["POST", "GET"],
        website=True
    )
    def remove_build(self, build, search='', **post):
        project = build.project_id
        build.sudo().with_delay().remove()
        project_url = "/sass_projects/%s" % slug(project)
        return request.redirect(request.httprequest.referrer or project_url)


    @http.route(
        [
            '/server/restart'
        ],
        type="http",
        auth="user",
        methods=["GET"],
        website=True
    )
    def kill_process(self, **kw):
        thread = RestartServer()
        thread.start()
        return request.redirect("/")

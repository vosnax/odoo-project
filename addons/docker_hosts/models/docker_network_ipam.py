# -*- coding: utf-8 -*-
from odoo import models, fields, api


class DockerNetworkIPAMPool(models.Model):
    _inherit = "docker.network.ipam.pool"

    ipam_id = fields.Many2one(
        'docker.network.ipam',
        string="IPAM Configuration",
        ondelete="cascade",
    )


class DockerNetworkIPAM(models.Model):
    _inherit = "docker.network.ipam"

    name = fields.Char(
        string="Name",
        help=(
            "Create an IPAM (IP Address Management) config dictionary "
            "to be used"
        ),
    )

    driver_id = fields.Many2one(
        "docker.driver",
        string="Driver",
        domain=[['type', '=', 'ipam']],
    )

    pool_ids = fields.One2many(
        'docker.network.ipam.pool',
        'ipam_id',
        string="Pool Configs",
    )

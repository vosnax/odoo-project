# -*- coding: utf-8 -*-
from odoo import models, api, fields


class DockerEndpointPort(models.Model):
    _inherit = "docker.endpoint.port"

    protocol = fields.Char(string="Protocol", default="tcp")
    target_port = fields.Integer(
        string="Target Port",
        help="The internal port of the running service"
    )
    published_port = fields.Integer(
        string="Published Port",
        help="The port published to the host"
    )

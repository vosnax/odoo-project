# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class DockerPlacementPreference(models.Model):
    _inherit = "docker.placement.preference"

    strategy = fields.Selection(
        selection=[
            ('spread', _('Spread')),
        ],
        string="Strategy",
        help=(
            "The placement strategy to implement. Currently, the only "
            "supported strategy is spread."
        )
    )

    descriptor = fields.Char(
        string="Descriptor",
        help=(
            "A label descriptor. For the spread strategy, the scheduler "
            "will try to spread tasks evenly over groups of nodes "
            "identified by this label."
        )
    )

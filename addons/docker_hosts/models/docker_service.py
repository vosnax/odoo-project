# -*- coding: utf-8 -*-
from odoo import fields, api, models, _
import docker
from docker.types import (
    DriverConfig,
    ServiceMode,
    SecretReference,
    UpdateConfig,
    RollbackConfig
)
from docker.types.services import EndpointSpec
import time
import logging
import ast
import shlex

log = logging.getLogger(__name__)


class DockerMount(models.Model):
    _inherit = "docker.mount"

    service_id = fields.Many2one(
        'docker.service',
        string="Service",
        ondelete="cascade"
    )


class DockerEnv(models.Model):
    _inherit = "docker.env"

    service_id = fields.Many2one(
        'docker.service',
        string="Service",
        ondelete="cascade"
    )


class DockerSecret(models.Model):
    _inherit = "docker.secret"

    service_id = fields.Many2one(
        'docker.service',
        string="Docker Secret",
        ondelete="cascade"
    )


class DockerLabel(models.Model):
    _inherit = "docker.label"

    service_id = fields.Many2one(
        'docker.service',
        string="Docker Service",
        ondelete="cascade"
    )


class DockerService(models.Model):
    """
    Docker Service

    A docker service implementation in odoo.
    """
    _inherit = "docker.service"

    name = fields.Char(
        string="Name",
        required=True,
        copy=False,
    )
    command = fields.Char(
        string="Command",
        help="Entrypoint o not override"
    )
    args = fields.Char(
        string="Args",
        help="Command line to be called into the containers"
    )
    image_id = fields.Many2one(
        'docker.image',
        string="Image",
        required=True
    )

    network_ids = fields.Many2many(
        'docker.network',
        string="Network",
    )

    secret_ids = fields.One2many(
        'docker.secret',
        'service_id',
        string="Secrets",
        copy=True,
    )

    mount_ids = fields.One2many(
        'docker.mount',
        'service_id',
        string='Mounts',
        copy=True,
    )

    env_ids = fields.One2many(
        'docker.env',
        'service_id',
        string="Environment Variables",
        copy=True
    )

    hostname = fields.Char(
        string="Hostname",
        required=True
    )

    published_ports = fields.Text(
        string="Ports",
        help="1 mapping per line"
    )

    service_mode = fields.Selection(
        selection=[
            ("replicated", _("Replicated")),
            ("global", _("Global"))
        ],
        string="Service Mode",
        default="replicated",
    )
    replicas = fields.Integer(
        string="Scale",
        default=1,
        required=True
    )

    label_ids = fields.One2many(
        'docker.label',
        'service_id',
        string="Labels",
        help="Labels to apply to the service.",
        copy=True
    )

    stop_grace_period = fields.Integer(
        string="Stop Grace Period",
        help=(
            "Amount of time in seconds to wait before "
            "forcefully kill a process"
        ),
        default=10,
        required=True
    )

    user = fields.Char(
        string="User",
        help="User to run commands as."
    )

    workdir = fields.Char(
        string="Work directory",
        help="Working directory for commands to run."
    )

    tty = fields.Boolean(
        string="TTY",
        help="Whether a pseudo-TTY should be allocated.",
        default=False,
    )

    open_stdin = fields.Boolean(
        string="Open STDIN",
        help="Open stdin",
        default=False,
    )

    read_only = fields.Boolean(
        string="Readonly container",
        help="Mount the container’s root filesystem as read only.",
        default=False
    )

    stop_signal = fields.Selection(
        selection=[
            ("SIGTERM", "SIGTERM"),
            ("SIGQUIT", "SIGQUIT"),
            ("SIGKILL", "SIGKILL"),
            ("SIGSEGV", "SIGSEGV"),
        ],
        string="Stop Signal",
        help="Set signal to stop the service’s containers",
        default="SIGTERM"
    )

    placement_id = fields.Many2one(
        "docker.placement",
        string="Placement",
        help="Placement constraints to be used as part of a TaskTemplate"
    )

    service_init = fields.Boolean(
        string="Init",
        help=(
            "Run an init inside the container that forwards signals and "
            "reaps processes"
        ),
        default=False
    )

    endpoint_id = fields.Many2one(
        "docker.endpoint",
        string="Endpoint"
    )

    rollback_config_id = fields.Many2one(
        "docker.service.config",
        # domain=[['type', '=', 'rollback']],
        string="Rollback Config",
    )

    update_config_id = fields.Many2one(
        "docker.service.config",
        string="Update Config",
    )

    @api.multi
    def copy(self, default=None):
        self.ensure_one()
        chosen_name = default.get('name')
        if not chosen_name:
            raise NotImplementedError(
                'Cannot duplicate a service without a specifying a new name'
            )
        return super(DockerService, self).copy(default=default)

    @api.model
    def create(self, vals):
        if 'docker_id' in vals:
            existing_service = self.search([
                ['docker_id', '=', vals['docker_id']]
            ], limit=1)
            if existing_service:
                return existing_service

        self_ctx = self.with_context(new_service=True)
        res = super(DockerService, self_ctx).create(vals)

        if not res.docker_id:
            res.create_service()

        return res

    @api.multi
    def write(self, vals):
        res = super(DockerService, self).write(vals)
        for obj in self:
            if obj.docker_id and not self.env.context.get('new_service'):
                obj.update_service()
            if not obj.docker_id:
                obj.create_service()
        return res

    @api.multi
    def unlink(self):
        docker_ids = self.mapped('docker_id')

        res = super(DockerService, self).unlink()

        client = docker.from_env()
        for docker_id in docker_ids:
            if docker_id:
                try:
                    service = client.services.get(docker_id)
                    service.remove()
                except docker.errors.NotFound:
                    pass
        return res

    @api.multi
    def get_environment_variables_dict(self):
        """
        Get a dict made from environment variables.
        """
        envs = {
            env.key: env.value
            for env in self.env_ids
        }
        return envs

    @api.multi
    def get_environment_variables(self):
        """
        Get Key Values in key=value format based
        on dict.
        """
        self.ensure_one()

        key_value = self.get_environment_variables_dict()

        variables = [
            "%s=%s" % (key, value)
            for key, value in key_value.items()
        ]

        return variables

    def get_secrets(self):
        self.ensure_one()

        secrets = [
            SecretReference(
                secret.docker_id,
                secret.secret_name,
                secret.filename,
                secret.uid,
                secret.gid,
                secret.mode_int
            )
            for secret in self.secret_ids
        ]

        return secrets

    def get_mounts(self):
        self.ensure_one()

        mounts = [
            "%s:%s:%s" % (mnt.source, mnt.target, mnt.read_access)
            for mnt in self.mount_ids
        ]

        return mounts

    def get_networks(self):
        self.ensure_one()

        networks = self.mapped('network_ids.docker_id')

        return networks

    def get_endpoint_spec(self):
        self.ensure_one()
        port_map = [
            {
                "Protocol": port.protocol,
                "TargetPort": port.target_port,
                "PublishedPort": port.published_port
            }
            for port in self.endpoint_id.port_ids
        ]
        spec = EndpointSpec(mode='vip', ports=port_map)
        return spec

    @api.multi
    def get_service_mode(self):
        self.ensure_one()

        mode = ServiceMode(
            mode=self.service_mode,
            replicas=self.replicas
        )

        return mode

    @api.multi
    def _prepare_create_vals(self, update=False):
        self.ensure_one()

        def maybe_set(params, key, value):
            if value:
                params[key] = value

        mounts = self.get_mounts()
        env_lst = self.get_environment_variables()
        labels = self.get_labels()
        secrets = self.get_secrets()
        networks = self.get_networks()
        spec = self.get_endpoint_spec()
        mode = self.get_service_mode()

        placement_id = self.placement_id

        if placement_id:
            constraints = placement_id.get_constraints()
            preferences = placement_id.get_preferences()
            platforms = placement_id.get_platforms()
        else:
            # required to reset the constraints
            constraints = []
            preferences = []
            platforms = []

        rollback_config = (
            RollbackConfig(**self.rollback_config_id.get_values())
            if self.rollback_config_id
            else None
        )

        update_config = (
            UpdateConfig(**self.update_config_id.get_values())
            if self.update_config_id
            else None
        )

        params = {
            "image": self.image_id.tag,
            "constraints": constraints,
            "preferences": preferences,
            "platforms": platforms,
            # "container_labels": ...
            "endpoint_spec": spec,
            "env": env_lst,
            "hostname": self.hostname,
            # "init": self.service_init, not supported < 1.38
            "labels": labels,
            # "log_driver": ... self.log_driver_id.driver
            # "log_driver_options": ... self.log_driver_id.get_options()
            "mode": mode,
            "mounts": mounts,
            "name": self.name,
            "networks": networks,
            # "resources": Resources docker.service.resources
            # "restart_policy": RestartPolicy docker.service.restart_policy
            "secrets": secrets,
            "stop_grace_period": self.stop_grace_period,
            "open_stdin": self.open_stdin,
            "read_only": self.read_only,
            "stop_signal": self.stop_signal,
            # "healthcheck": HealthCheck docker.service.healthcheck
            # "hosts": docker.service.hosts
            # "dns_config":  ...
            # "config": config_ids: docker.config
            # "privileges": docker.service.privileges
        }

        optional_params = {
            "command": shlex.split(self.command or ''),
            "args": shlex.split(self.args or ''),
            "user": self.user,
            "workdir": self.workdir,
            "tty": self.tty,
            "update_config": update_config,
            "rollback_config": rollback_config,
            # "groups": [...]
        }

        for key, value in optional_params.items():
            maybe_set(params, key, value)

        if update:
            params['force_update'] = True

        return params

    @api.multi
    def create_service(self):
        self.ensure_one()
        client = docker.from_env()

        vals = self._prepare_create_vals()
        service = client.services.create(**vals)
        self.docker_id = service.id

    @api.multi
    def update_service(self):
        self.ensure_one()
        client = docker.from_env()

        vals = self._prepare_create_vals(update=True)
        try_again = 5
        while try_again:
            try:
                service = client.services.get(self.docker_id)
                service.update(**vals)
                try_again = 0
            except docker.errors.NotFound:
                # Service is can't be found, we can't update it
                log.info('Not found cause unlink')
                self.unlink()
            except docker.errors.APIError as exc:
                log.info(exc)
                log.info('Failed to update trying again in 1s')
                time.sleep(1)
                try_again -= 1

    @api.multi
    def scale(self):
        self.ensure_one()
        client = docker.from_env()

        for obj in self:
            service = client.services.get(obj.docker_id)
            if not service.scale(obj.replicas):
                raise Warning("Couldn't scale service")

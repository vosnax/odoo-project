# -*- coding: utf-8 -*-
from odoo import api, models, fields


class DockerLabel(models.Model):
    _inherit = 'docker.env'

    key = fields.Char(string="Label")
    value = fields.Char(string="Value")

    @api.multi
    def name_get(self):
        names = [
            (obj.id, "%s=%s" % (obj.key, obj.value))
            for obj in self
        ]
        return names

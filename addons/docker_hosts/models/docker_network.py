# -*- coding: utf-8 -*-
from odoo import api, tools, models, fields, _
import docker


class DockerLabel(models.Model):
    _inherit = "docker.label"

    network_id = fields.Many2one(
        'docker.network',
        string="Network",
        ondelete="cascade",
    )


class DockerNetwork(models.Model):
    _inherit = 'docker.network'

    name = fields.Char(string='Name')

    ipam_id = fields.Many2one(
        "docker.network.ipam",
        string="IPAM",
    )

    check_duplicate = fields.Boolean(
        string="Check Duplicates",
        default=False,
        help="Request daemon to check for networks with same name",
    )

    internal = fields.Boolean(
        string="Internal",
        help=(
            "Restrict external access to the network."
        ),
        default=False
    )

    attachable = fields.Boolean(
        string="Attachable",
        default=False,
        help=(
            "If enabled, and the network is in the global scope, "
            "non-service containers on worker nodes will be able "
            "to connect to the network."
        )
    )

    scope = fields.Selection(
        selection=[
            ('default', 'Default'),
            ('local', 'Local'),
            ('global', 'Global'),
            ('swarm', 'Swarm')
        ],
        string="Scope",
        help="Specify the network’s scope",
        default="default"
    )

    ingress = fields.Selection(
        selection=[
            ('default', 'Default'),
            ('true', 'True'),
            ('false', 'False')
        ],
        string="Ingress",
        help=(
            "If set, create an ingress network which provides the "
            "routing-mesh in swarm mode."
        ),
        default=False
    )

    driver_id = fields.Many2one(
        'docker.driver',
        domain=[['type', '=', 'network']],
        string="Driver",
        help="Name of the driver used to create the network"
    )

    label_ids = fields.One2many(
        'docker.label',
        'network_id',
        string="Labels",
        help="Map of labels to set on the network.",
        copy=True,
    )

    @api.model
    def create(self, vals):
        res = super(DockerNetwork, self).create(vals)
        if not res.docker_id:
            res.create_network()
        return res

    @api.multi
    def unlink(self):
        client = docker.from_env()
        docker_ids = self.mapped('docker_id')

        ipam_ids = self.mapped('ipam_id')
        ipam_ids.unlink()

        res = super(DockerNetwork, self).unlink()

        for docker_id in docker_ids:
            network = client.networks.get(docker_id)
            network.remove()

        return res

    @api.multi
    def create_network(self):
        client = docker.from_env()
        for obj in self:
            if obj.docker_id:
                continue

            driver = obj.driver_id.driver
            driver_options = obj.driver_id.get_options()
            labels = {
                rec.key: rec.value
                for rec in obj.label_ids
            }
            check_duplicate = obj.check_duplicate
            internal = obj.internal
            enable_ipv6 = obj.enable_ipv6
            attachable = obj.attachable
            scope = obj.scope
            ingress = obj.ingress

            config = {
                "name": obj.name,
                "driver": driver,
                "options": driver_options,
                "check_duplicate": check_duplicate,
                "internal": internal,
                "labels": labels,
                "enable_ipv6": enable_ipv6,
                "attachable": attachable,
            }

            if scope != 'default':
                config['scope'] = scope

            if ingress != 'default':
                config['ingress'] = (ingress == 'true')

            try:
                net = client.networks.get(obj.name)
            except Exception:
                net = client.networks.create(**config)

            obj.docker_id = net.id

    @api.multi
    def destroy_network(self):
        self.ensure_one()

        client = docker.from_env()

        try:
            net = client.networks.get(self.docker_id)
        except Exception:
            net = None

        if net:
            net.remove()

        self.docker_id = None

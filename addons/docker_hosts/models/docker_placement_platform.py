# -*- coding: utf-8 -*-
from odoo import api, models, fields


class DockerPlacementPlatform(models.Model):
    _inherit = "docker.placement.platform"

    arch = fields.Char(string="Arch")
    os = fields.Char(string="OS")

# -*- coding: utf-8 -*-
from odoo import models, fields, api

import docker


class DockerObjectMixin(models.AbstractModel):
    _name = "docker.object.mixin"
    _description = "Docker Object Mixin"

    docker_id = fields.Char(
        string="Docker Object ID",
        copy=False
    )

    @api.model
    def find_existing_object(self, docker_id):
        obj = self.search(
            [
                ['docker_id', '=', docker_id],
            ],
            limit=1
        )
        return obj

    @api.multi
    def docker_create(self):
        values = self.prepare_docker_vals()
        client = docker.from_env()
        return client[self._docker_object].create(values)

    @api.multi
    def docker_obj(self):
        self.ensure_one()
        client = docker.from_env()
        return client[self._docker_object].get(self.docker_id)

    @api.multi
    def docker_remove(self):
        self.ensure_one()
        docker_obj = self.docker_obj()
        docker_obj.remove()

    @api.multi
    def prepare_docker_vals(self):
        raise NotImplementedError()


class DockerObjectUpdateMixin(models.AbstractModel):
    _name = "docker.object.update.mixin"
    _description = "Docker Object Update Mixin"

    @api.multi
    def prepare_docker_update_vals(self):
        vals = self.prepare_docker_vals()
        return vals

    @api.multi
    def docker_update(self):
        self.ensure_one()
        values = self.prepare_docker_update_vals()
        docker_obj = self.docker_obj()
        docker_obj.update(values)
        return docker_obj


class DockerLabelsMixin(models.AbstractModel):
    _name = "docker.labels.mixin"
    _description = "Docker Label Mixin"

    @api.multi
    def get_labels(self):
        self.ensure_one()

        return {
            lbl.key: lbl.value
            for lbl in self.label_ids
        }

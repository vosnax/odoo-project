# -*- coding: utf-8 -*-
from odoo import api, models, fields


class DockerPlacementConstraint(models.Model):
    _inherit = "docker.placement.constraint"

    placement_id = fields.Many2one(
        "docker.placement"
    )


class DockerPlacementPreference(models.Model):
    _inherit = "docker.placement.preference"

    placement_id = fields.Many2one(
        "docker.placement"
    )


class DockerPlacement(models.Model):
    _inherit = "docker.placement"

    constraint_ids = fields.One2many(
        "docker.placement.constraint",
        "placement_id",
        string="Constraints",
        copy=True,
    )

    preference_ids = fields.One2many(
        "docker.placement.preference",
        "placement_id",
        string="Preferences",
        copy=True,
    )

    platform_ids = fields.Many2many(
        "docker.placement.platform",
        string="Platforms",
        copy=True
    )

    @api.multi
    def get_constraints(self):
        self.ensure_one()

        constraints = [
            constraint.name
            for constraint in self.constraint_ids
        ]

        return constraints

    @api.multi
    def get_preferences(self):
        self.ensure_one()

        prefs = [
            (pref.strategy, pref.descriptor)
            for pref in self.preference_ids
        ]

        return prefs

    @api.multi
    def get_platforms(self):
        self.ensure_one()

        platforms = [
            (platform.arch, platform.os)
            for platform in self.platform_ids
        ]

        return platforms

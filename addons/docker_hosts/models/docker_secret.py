from odoo import api, models, fields
from ast import literal_eval

import docker


class DockerLabels(models.Model):
    _inherit = "docker.label"

    secret_id = fields.Many2one(
        "docker.secret",
        string="Secret"
    )


class DockerSecret(models.Model):
    _inherit = "docker.secret"

    name = fields.Char(
        string="Name",
        required=True,
    )
    secret_name = fields.Char(
        string="Secret Name",
        compute="compute_secret_name",
    )
    filename = fields.Char(
        string="Filename",
        required=True
    )
    uid = fields.Char(string="UID")
    gid = fields.Char(string="GID")
    mode = fields.Char(string="Mode", help="Integer in octal base")
    # TODO add binary data
    data = fields.Text(string="Data", required=True)

    mode_int = fields.Integer(
        compute="compute_mode_int",
        string="Mode int"
    )

    driver_id = fields.Many2one(
        'docker.driver',
        string="Driver"
    )
    label_ids = fields.One2many(
        'docker.label',
        'secret_id',
        string="Labels",
        copy=True,
    )

    @api.model
    def create(self, vals):
        if 'docker_id' in vals:
            obj = self.search(
                [
                    ['docker_id', '=', vals['docker_id']],
                ],
                limit=1
            )
            if obj:
                return obj

        obj = super(DockerSecret, self).create(vals)

        obj.create_secret()

        return obj

    @api.multi
    def write(self, vals):
        res = super(DockerSecret, self).write(vals)
        self.create_secret()
        return res

    @api.multi
    def unlink(self):
        client = docker.from_env()
        docker_ids = self.mapped('docker_id')
        res = super(DockerSecret, self).unlink()

        for docker_id in docker_ids:
            try:
                secret = client.secrets.get(docker_id)
                secret.remove()
            except Exception:
                pass

        return res

    @api.multi
    @api.depends('service_id', 'name')
    def compute_secret_name(self):
        for obj in self:
            name = obj.name
            secret_name = (
                "%s-%s" % (obj.service_id.name, name)
                if obj.service_id
                else name
            )
            obj.secret_name = secret_name

    @api.multi
    def create_secret(self):
        client = docker.from_env()

        for obj in self:
            if obj.docker_id:
                continue

            labels = obj.get_labels()
            driver = obj.get_driver()
            data = obj.data

            secret_name = obj.secret_name

            dc_secret = client.secrets.create(
                name=secret_name,
                labels=labels,
                driver=driver,
                data=data
            )

            dc_secret.reload()

            obj.docker_id = dc_secret.id

    @api.multi
    def get_driver(self):
        self.ensure_one()
        return (
            None if not self.driver_id
            else self.driver_id.get_config()
        )

    @api.depends("mode")
    def compute_mode_int(self):
        for obj in self:
            obj.mode_int = literal_eval(obj.mode or '0')

    @api.constrains("mode")
    def check_is_int(self):
        for obj in self:
            try:
                val = literal_eval(obj.mode)
                if not isinstance(val, int):
                    raise ValueError("Mode should be of the format 0o777")
            except SyntaxError:
                raise ValueError("Mode should be of the format 0o777")

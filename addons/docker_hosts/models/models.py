# -*- coding: utf-8 -*-
from odoo import models, fields, api


#   class DockerLabelsMixin(models.AbstractModel):
#       _name = 'docker.labels.mixin'
#       _description = "Docker Labels Mixin"
#
#
#   class DockerObjectMixin(models.AbstractModel):
#       _name = 'docker.object.mixin'
#       _description = "Docker Object Mixin"
#
#
#   class DockerObjectUpdateMixin(models.AbstractModel):
#       _name = 'docker.object.update.mixin'
#       _description = "Docker Object Update Mixin"


class DockerDriverOption(models.Model):
    _name = "docker.driver.option"
    _description = "Docker Driver Option"
    _rec_name = 'key'


class DockerDriver(models.Model):
    _name = "docker.driver"
    _description = "Docker Driver"


class DockerLabel(models.Model):
    _name = "docker.label"
    _description = "Docker Label"
    _rec_name = 'key'


class DockerSecret(models.Model):
    _name = "docker.secret"
    _inherit = ['docker.labels.mixin', 'docker.object.mixin']
    _description = "Docker Secret"


class DockerVolume(models.Model):
    _name = "docker.volume"
    _inherit = ['docker.labels.mixin', 'docker.object.mixin']
    _description = "Docker Volume"
    _docker_object = "volumes"


class DockerServiceConfig(models.Model):
    _name = "docker.service.config"
    _description = "Docker Service Config"


class DockerService(models.Model):
    _name = "docker.service"
    _inherit = [
        'docker.labels.mixin',
        'docker.object.mixin',
        'docker.object.update.mixin',
    ]
    _description = "Docker Service"
    _docker_object = "services"


class DockerMount(models.Model):
    _name = "docker.mount"
    _description = "Docker Mount"


class DockerNetwork(models.Model):
    _name = "docker.network"
    _inherit = ['docker.labels.mixin', 'docker.object.mixin']
    _description = "Docker Network"
    _docker_object = "networks"


class DockerNetworkIPAM(models.Model):
    _name = "docker.network.ipam"
    _description = "Docker Net IPAM"


class DockerNetworkIPAMPool(models.Model):
    _name = "docker.network.ipam.pool"
    _description = "Docker Net IPAM Pool"


class DockerNetworkAux(models.Model):
    _name = "docker.network.aux"
    _description = "Docker IPAM Pool Aux"


class DockerImage(models.Model):
    _name = "docker.image"
    _inherit = ['docker.labels.mixin', 'docker.object.mixin']
    _description = "Docker Image"
    _docker_object = "images"


class DockerContainer(models.Model):
    _name = "docker.container"
    _inherit = ['docker.object.mixin', 'docker.labels.mixin']
    _description = "Docker Container"
    _docker_object = "containers"


class DockerEndpoint(models.Model):
    _name = "docker.endpoint"
    _description = "Docker Endpoint"


class DockerEndpointPort(models.Model):
    _name = "docker.endpoint.port"
    _description = "Docker Endpoint Port"


class DockerEnv(models.Model):
    _name = "docker.env"
    _description = "Docker Environment Variables"


class DockerPlacement(models.Model):
    _name = "docker.placement"
    _description = "Docker Placement"


class DockerPlacementConstraint(models.Model):
    _name = "docker.placement.constraint"
    _description = "Docker Placement Constraint"


class DockerPlacementPreference(models.Model):
    _name = "docker.placement.preference"
    _description = "Docker Placement Preference"


class DockerPlacementPlatform(models.Model):
    _name = "docker.placement.platform"
    _description = "Docker Placement Platform"

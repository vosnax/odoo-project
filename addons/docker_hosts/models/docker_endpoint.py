# -*- coding: utf-8 -*-
from odoo import models, fields, api, _


class DockerEndpointPorts(models.Model):
    _inherit = "docker.endpoint.port"

    endpoint_id = fields.Many2one(
        'docker.endpoint',
        string="Endpoint",
        ondelete="cascade"
    )


class DockerEndpoint(models.Model):
    _inherit = "docker.endpoint"

    name = fields.Char(string="Name")

    mode = fields.Selection(
        selection=[
            ('vip', _('VIP')),
            ('dnsrr', _('DNSRR'))
        ],
        string="Mode"
    )

    port_ids = fields.One2many(
        'docker.endpoint.port',
        'endpoint_id',
        string="Ports"
    )

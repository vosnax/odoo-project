# -*- coding: utf-8 -*-
from odoo import api, models, fields


class DockerPlacementConstraint(models.Model):
    _inherit = "docker.placement.constraint"

    name = fields.Char(string="Constraint")

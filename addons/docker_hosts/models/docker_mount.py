# -*- coding: utf-8 -*-
from odoo import fields, api, models, _
import docker


class DockerMount(models.Model):
    _inherit = 'docker.mount'

    type = fields.Selection(
        selection=[
            ('bind', _('Bind')),
            ('volume', _('volume'))
        ],
        string="Type",
        default="volume"
    )

    volume_id = fields.Many2one(
        'docker.volume',
        string="Volume",
        copy=True,
    )

    source_txt = fields.Char(string="Bind Source")

    target = fields.Char(string="Target")
    source = fields.Char(
        compute="_compute_source",
        string="Source"
    )

    read_access = fields.Selection(
        selection=[
            ('rw', _('Read/Write')),
            ('ro', _('Read only'))
        ],
        string="Access",
        default="rw"
    )

    @api.depends('volume_id', 'source_txt', 'type')
    def _compute_source(self):
        for obj in self:
            if obj.type == 'bind':
                obj.source = obj.source_txt
            elif obj.type == 'volume' and obj.volume_id.docker_id:
                obj.source = obj.volume_id.docker_id

    @api.multi
    def name_get(self):
        names = [
            (
                obj.id,
                "%s -> %s : %s" % (
                    obj.source, obj.target, obj.read_access
                )
            )
            for obj in self
        ]
        return names

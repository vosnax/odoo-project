# -*- coding: utf-8 -*-
from odoo import api, fields, models
import docker
from io import BytesIO
from tempfile import TemporaryDirectory
from os import path


class DockerLabel(models.Model):
    _inherit = "docker.label"

    image_id = fields.Many2one(
        "docker.image",
        string="Image"
    )


class DockerImage(models.Model):
    _inherit = 'docker.image'
    _rec_name = 'tag'

    tag = fields.Char(string="Tag")

    label_ids = fields.One2many(
        "docker.label",
        "image_id",
        string="Labels",
        copy=True,
    )

    @api.multi
    def unlink(self):
        docker_ids = self.mapped('docker_id')
        res = super(DockerImage, self).unlink()
        client = docker.from_env()

        for docker_id in docker_ids:
            try:
                img = client.images.get(docker_id)
                img.remove()
            except docker.errors.NotFound:
                pass

        return res

    @api.multi
    def pull_image(self):
        client = docker.from_env()
        for obj in self:
            if obj.tag:
                image = client.images.pull(obj.tag)
                obj.docker_id = image.id
                obj.label_ids = [
                    (
                        0,
                        None,
                        {
                            "image_id": obj.id,
                            "key": key,
                            "value": value
                        }
                    )
                    for key, value in image.labels.items()
                ]

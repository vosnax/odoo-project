# -*- coding: utf-8 -*-
from odoo import models, fields, api


class DockerNetworkAux(models.Model):
    _inherit = "docker.network.aux"

    pool_id = fields.Many2one(
        "docker.network.ipam.pool",
        string="Pool",
        ondelete="cascade",
    )


class DockerNetworkIPAMPool(models.Model):
    _inherit = "docker.network.ipam.pool"

    subnet = fields.Char(
        string="Subnet",
        help="Custom subnet for this IPAM pool using the CIDR notation",
    )

    iprange = fields.Char(
        string="IP Range",
        help=(
            "Custom IP range for endpoints in this IPAM pool using "
            "the CIDR notation."
        ),
    )

    gateway = fields.Char(
        string="Gateway",
        help="Custom IP address for the pool’s gateway.",
    )

    aux_addresses_ids = fields.One2many(
        'docker.network.aux',
        'pool_id',
        string="Aux Addresses",
        help="Key reserved IPs"
    )

# -*- coding: utf-8 -*-
from odoo import api, models, fields, _

import docker
from docker.types import (
    DriverConfig
)


class DockerDriverOption(models.Model):
    _inherit = 'docker.driver.option'

    key = fields.Char(string="Option")
    value = fields.Char(string="Value")
    driver_id = fields.Many2one(
        'docker.driver',
        string="Volume"
    )


class OdooDriverConfig(models.Model):
    _inherit = "docker.driver"

    name = fields.Char(string="Name")

    driver = fields.Char(string="Driver")

    type = fields.Selection(
        selection=[
            ('volume', _('Volume')),
            ('secret', _('Secret')),
            ('network', _('Network')),
            ('ipam', _('IPAM')),
        ],
        string="Driver Type"
    )

    option_ids = fields.One2many(
        'docker.driver.option',
        'driver_id',
        string="Options",
        copy=True,
    )

    def get_config(self):
        self.ensure_one()
        return DriverConfig(
            name=self.name,
            options=self.get_options()
        )

    def get_options(self):
        self.ensure_one()

        return {
            opt.key: opt.value
            for opt in self.option_ids
        }

from odoo import api, models, fields


class DockerLabel(models.Model):
    _inherit = 'docker.label'

    key = fields.Char(string="Label")
    value = fields.Char(string="Value")

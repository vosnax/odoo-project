# -*- coding: utf-8 -*-
from odoo import api, tools, models, fields, _
import docker


class DockerLabels(models.Model):
    _inherit = 'docker.label'

    volume_id = fields.Many2one(
        'docker.volume',
        string="Volume",
        ondelete="cascade",
    )


class DockerVolume(models.Model):
    _inherit = 'docker.volume'

    name = fields.Char(string='Name')
    driver_id = fields.Many2one(
        "docker.driver",
        domain=[['type', '=', 'volume']],
        string="Docker Driver"
    )
    label_ids = fields.One2many(
        'docker.label',
        'volume_id',
        string="Options",
        copy=True
    )

    @api.multi
    def copy(self, default=None):
        # TODO implement a way to detect a service module and
        # prepend/replace prefix to prevent duplicate volumes
        # same story as services because volumes
        # cannot be renamed...
        self.ensure_one()
        chosen_name = default.get('name')
        if not chosen_name:
            raise NotImplementedError(
                'Cannot duplicate a volume without a specifying a new name'
            )
        return super(DockerVolume, self).copy(default=default)

    @api.model
    def create(self, vals):
        if 'docker_id' in vals:
            volume = self.search([
                ['docker_id', '=', vals['docker_id']]
            ], limit=1)
            if volume:
                return volume

        res = super(DockerVolume, self).create(vals)
        if not res.docker_id:
            res.create_volume()

        return res

    @api.multi
    def unlink(self):
        client = docker.from_env()
        docker_ids = self.mapped('docker_id')
        res = super(DockerVolume, self).unlink()

        # Destroy volumes only when unlink works
        for docker_id in docker_ids:
            if docker_id:
                try:
                    volume = client.volumes.get(docker_id)
                    volume.remove()
                except docker.errors.NotFound:
                    pass

        return res

    @api.multi
    def create_volume(self):
        client = docker.from_env()
        for obj in self:
            if obj.docker_id:
                continue

            options = (
                obj.driver_id.get_options()
                if obj.driver_id
                else None
            )

            driver = (
                'local'
                if not obj.driver_id
                else obj.driver_id.driver
            )

            vol = client.volumes.create(
                name=obj.name,
                driver=driver,
                driver_opts=options,
                labels=obj.get_labels()
            )

            obj.docker_id = vol.id

    @api.multi
    def destroy_volume(self):
        self.ensure_one()

        client = docker.from_env()

        try:
            vol = client.volumes.get(self.docker_id)
        except Exception:
            vol = None

        if vol:
            vol.remove()

        self.docker_id = None

    @api.multi
    def copy_to(self, volume_id, detach=True):
        """
        Copy this volume to an other one.
        """
        client = docker.from_env()
        volumes = {
            self.name: {
                'bind': '/mntA',
                'mode': 'ro'
            },
            volume_id.name: {
                'bind': '/mntB',
                'mode': 'rw',
            }
        }
        client.containers.run(
            'debian:stretch',
            'cp -r /mntA/. /mntB',
            auto_remove=True,
            detach=detach,
            volumes=volumes
        )

    @api.multi
    def copy_from(self, directory, detach=False):
        client = docker.from_env()
        volumes = {
            directory: {
                'bind': '/mntA',
                'mode': 'ro'
            },
            self.name: {
                'bind': '/mntB',
                'mode': 'rw',
            }
        }
        client.containers.run(
            'debian:stretch',
            'cp -r /mntA/. /mntB',
            detach=detach,
            volumes=volumes
        )

    @api.multi
    def empty(self, detach=False):
        client = docker.from_env()
        volumes = {
            self.name: {
                'bind': '/mntB',
                'mode': 'rw',
            }
        }
        client.containers.run(
            'debian:stretch',
            'find /mntB -mindepth 1 -delete',
            auto_remove=True,
            detach=detach,
            volumes=volumes
        )

# -*- coding: utf-8 -*-
from odoo import fields, api, models, _


class DockerServiceRollback(models.Model):
    _inherit = "docker.service.config"

    name = fields.Char(
        string="Name",
    )

    # type = fields.Selection(
    #     selection=[
    #         ('rollback', _('Rollback')),
    #         ('update', _('Update'))
    #     ],
    #     string="Config Type"
    # )

    parallelism = fields.Integer(
        string="Parallelism",
        default=0,
        help="Maximum number of tasks to be rolled back in one iteration"
    )

    delay = fields.Integer(
        string="Delay",
        help="Amount of time between rollbacks, in nanoseconds."
    )

    failure_action = fields.Selection(
        selection=[
            ('continue', _('Continue')),
            ('pause', _('Pause')),
            ('rollback', _('Rollback'))
        ],
        string="Failure Action",
        help=(
            "Action to take if a rolled back task fails to run, "
            "or stops running during the rollback. Acceptable "
            "values are continue, pause or rollback."
        )
    )

    monitor = fields.Integer(
        string="Monitor",
        help=(
            "Amount of time to monitor each rolled back task for "
            "failures, in nanoseconds."
        )
    )

    max_failure_ratio = fields.Float(
        string="Max Failure Ration",
        help=(
            "The fraction of tasks that may fail during a rollback "
            "before the failure action is invoked, specified as a "
            "floating point number between 0 and 1."
        ),
        default=0
    )

    order = fields.Selection(
        selection=[
            ("start_first", _("Start First")),
            ("stop_first", _("Stop First")),
        ],
        string="Update Order",
        help=(
            "Specifies the order of operations when rolling out a "
            "rolled back task."
        )
    )

    @api.multi
    def get_values(self):
        self.ensure_one()

        return {
            "parallelism": self.parallelism,
            "delay": self.delay,
            "failure_action": self.failure_action,
            "monitor": self.monitor,
            "max_failure_ratio": self.max_failure_ratio,
            "order": self.order,
        }

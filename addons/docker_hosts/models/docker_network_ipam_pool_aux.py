# -*- coding: utf-8 -*-
from odoo import models, fields, api


class DockerNetworkAux(models.Model):
    _inherit = "docker.network.aux"

    name = fields.Char(string="Key")
    value = fields.Char(string="IP")

# -*- coding: utf-8 -*-
from odoo import fields, api, models, _
import docker


class DockerImage(models.Model):
    _inherit = "docker.label"

    container_id = fields.Many2one(
        "docker.image",
        string="Container"
    )


class DockerMount(models.Model):

    _inherit = "docker.mount"

    # Probably Many2many as a mount point should
    # be defined on a service and used on many
    # containers of the same services...
    container_id = fields.Many2one(
        'docker.container',
        string="Container"
    )


class DockerContainer(models.Model):
    _inherit = "docker.container"

    name = fields.Char(string="Name")

    image_id = fields.Many2one('docker.image', string="Image")

    network_id = fields.Many2one(
        'docker.network',
        string="Network"
    )

    mount_ids = fields.One2many(
        'docker.mount',
        'container_id',
        string='Mounts'
    )

    environment_variables = fields.Text(
        string="Envs", help="1 key valuer per line"
    )

    restart = fields.Selection(
        [
            ('always', _('Always')),
        ],
        string="Restart"
    )

    published_ports = fields.Text(
        string="Ports",
        help="1 mapping per line"
    )

    label_ids = fields.One2many(
        'docker.label',
        'container_id',
        string="Labels"
    )

    @api.multi
    def unlink(self):
        docker_ids = self.mapped('docker_id')
        res = super(DockerContainer, self).unlink()

        client = docker.from_env()
        for docker_id in docker_ids:
            container = client.containers.get(docker_id)
            container.stop()
            container.remove()

        return res

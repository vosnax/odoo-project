# -*- coding: utf-8 -*-
{
    'name': 'Docker Hosts',
    'version': '11.0.0.1.0',
    'author': 'Loïc Faure-Lacroix',
    'maintainer': '',
    'website': 'http://archeti.ca',
    'license': 'AGPL-3',
    'category': 'Others',
    'summary': '',
    'description': """
""",
    'depends': [
        'base',
    ],
    'external_dependencies': {
        'python': [],
    },
    'data': [
        'actions.xml',
        'menus/menus.xml',
        'views/docker_service.xml',
        'views/docker_container.xml',
        'views/docker_mount.xml',
        'views/docker_volume.xml',
        'views/docker_label.xml',
        'views/docker_image.xml',
        'views/docker_network.xml',
        'views/docker_secret.xml',
        'views/docker_network_ipam.xml',
        'views/docker_network_ipam_pool.xml',
        'views/docker_network_ipam_aux.xml',
        'views/docker_env.xml',
        'security/groups.xml',
        'security/acl.xml',
    ],
    'installable': True,
    'application': True,
}

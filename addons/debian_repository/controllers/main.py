# -*- coding: utf-8 -*-
import lzma
from odoo import http, models, fields, _
from odoo.http import request, Response
from odoo.tools import pycompat, OrderedSet
from odoo.addons.http_routing.models.ir_http import slug, _guess_mimetype
from odoo.addons.web.controllers.main import WebClient, Binary
from odoo.addons.portal.controllers.portal import pager as portal_pager
from odoo.addons.portal.controllers.web import Home

from base64 import b64decode


class Website(Home):

    @http.route(
        '/debian/dists/<dist>/Release',
        type='http',
        auth="public",
        website=True
    )
    def get_dist_release(self, dist, *args, **kargs):
        dist_obj = request.env['debian.distribution']
        distrib = dist_obj.sudo().search([
            ['name', '=', dist],
        ])
        return Response(
            distrib.get_release(),
            content_type='text/plain; charset=utf-8',
        )

    @http.route(
        '/debian/dists/<dist>/server.key',
        type='http',
        auth="public",
        website=True
    )
    def get_dist_key(self, dist, *args, **kwargs):
        dist_obj = request.env['debian.distribution']
        distrib = dist_obj.sudo().search([
            ['name', '=', dist],
        ])
        return Response(
            distrib.gpg_key_id.public_key,
            content_type='text/plain; charset=utf-8',
        )

    @http.route(
        '/debian/dists/<dist>/Release.gpg',
        type='http',
        auth="public",
        website=True
    )
    def get_dist_release_gpg(self, dist, *args, **kargs):
        dist_obj = request.env['debian.distribution']
        distrib = dist_obj.sudo().search([
            ['name', '=', dist],
        ])
        release = distrib.get_release()
        return Response(
            distrib.gpg_key_id.sign(release, True),
            content_type='text/plain; charset=utf-8',
        )

    @http.route(
        '/debian/dists/<dist>/InRelease',
        type='http',
        auth="public",
        website=True
    )
    def get_dist_inrelease(self, dist, *args, **kargs):
        dist_obj = request.env['debian.distribution']
        distrib = dist_obj.sudo().search([
            ['name', '=', dist],
        ])
        release = distrib.get_release()
        return Response(
            distrib.gpg_key_id.sign(release),
            content_type='text/plain; charset=utf-8',
        )

    def get_arch_obj(self, dist, component, arch):
        component_obj = request.env['debian.architecture']

        real_component = component_obj.sudo().search(
            [
                ['path_name', '=', arch],
                ['component_id.name', '=', component],
                ['component_id.distribution_id.name', '=', dist]
            ],
            limit=1
        )

        return real_component

    @http.route(
        '/debian/dists/<dist>/<component>/<arch>/Release',
        type='http',
        auth="public",
        website=True
    )
    def get_component_release(self, dist, component, arch, *args, **kargs):
        real_component = self.get_arch_obj(dist, component, arch)
        data = real_component.get_release()
        return Response(
            data,
            content_type='text/plain; charset=utf-8',
        )

    @http.route(
        '/debian/dists/<dist>/<component>/<arch>/Packages.xz',
        type='http',
        auth="public",
        website=True
    )
    def get_component_packages_xz(self, dist, component, arch, *args, **kargs):
        real_component = self.get_arch_obj(dist, component, arch)
        data = real_component.get_packages_xz()
        return Response(
            data,
            content_type="application/x-xz"
        )

    @http.route(
        '/debian/dists/<dist>/<component>/<arch>/Packages.gz',
        type='http',
        auth="public",
        website=True
    )
    def get_component_packages_gz(self, dist, component, arch, *args, **kargs):
        real_component = self.get_arch_obj(dist, component, arch)
        data = real_component.get_packages_gz()
        return Response(
            data,
            content_type="application/x-gzip"
        )

    @http.route(
        '/debian/pool/p/<package_id>/<filename>',
        type='http',
        auth='public',
        website=True
    )
    def get_file_archive(self, package_id, filename, *args, **kargs):
        pack_obj = request.env['debian.package']
        package = pack_obj.sudo().browse(int(package_id))
        return Response(
            b64decode(package.debfile),
            content_type="application/x-debian-package"
        )

# -*- coding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api, _
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from os import path

import lzma
import gzip
from collections import defaultdict, OrderedDict
from hashlib import md5, sha1, sha256
from datetime import datetime
import pytz
from io import BytesIO

from debian.arfile import ArFile
from base64 import b64decode
import tarfile

checksum_func = OrderedDict()

checksum_func['MD5Sum'] = lambda x: md5(x).hexdigest()
checksum_func['SHA1'] = lambda x: sha1(x).hexdigest()
checksum_func['SHA256'] = lambda x: sha256(x).hexdigest()


class DebianDistribution(models.Model):
    _name = 'debian.distribution'
    name = fields.Char(string='Name')
    origin = fields.Char(string="Origin")
    label = fields.Char(string="Label")
    suite = fields.Char(string="Suite")
    version = fields.Char(string="Version")
    codename = fields.Char(string="Codename")
    description = fields.Char(string="Description")
    gpg_key_id = fields.Many2one(
        'gpg.key',
        string="GPG Key"
    )

    @api.multi
    def get_release(self):
        architectures = self.env['debian.architecture'].search(
            [
                ['component_id.distribution_id', '=', self.id]
            ]
        )

        checksums = defaultdict(list)

        for arch in architectures:
            files = {
                'Release': arch.get_release(),
                'Packages.xz': arch.get_packages_xz(),
                'Packages.gz': arch.get_packages_gz(),
                'Packages': arch.get_packages()
            }

            base_url = path.join(
                arch.component_id.name,
                arch.path_name
            )

            for name, checksum in checksum_func.items():
                for filename, data in files.items():
                    checksums[name].append(
                        [
                            "",
                            checksum(data),
                            str(len(data)),
                            path.join(base_url, filename)
                        ]
                    )

        checksums2 = []
        for checksum, data in checksums.items():
            checksums2.append("%s:" % checksum)
            for line in data:
                checksums2.append(
                    " ".join(line)
                )

        components = " ".join(self.mapped('component_ids.name'))
        architectures = " ".join(
            self.mapped('component_ids.architecture_ids.name')
        )

        DATE_FORMAT = '%a, %-d %b %Y %H:%M:%S %Z'
        dt = self.write_date
        rd = datetime.strptime(dt, DEFAULT_SERVER_DATETIME_FORMAT)
        # date = rd.astimezone(pytz.utc).strftime(DATE_FORMAT)
        date = pytz.utc.localize(rd).strftime(DATE_FORMAT)

        data = """Origin: %(origin)s
Label: %(label)s
Suite: %(suite)s
Codename: %(codename)s
Date: %(date)s
Architectures: %(architectures)s
Components: %(components)s
Description: %(description)s
%(checksums)s
        """ % {
            "origin": self.origin,
            "label": self.label,
            "suite": self.suite,
            "version": self.version,
            "codename": self.codename,
            "architectures": architectures,
            "components": components,
            "description": self.description,
            "checksums": "\n".join(checksums2),
            "date": date
        }

        return data.encode()


class DebianComponent(models.Model):
    _name = 'debian.component'

    name = fields.Char(string='Name')

    distribution_id = fields.Many2one(
        'debian.distribution',
        string='Distribution'
    )


class DebianArchitecture(models.Model):
    _name = 'debian.architecture'
    name = fields.Char(string="Name")
    component_id = fields.Many2one(
        'debian.component',
        string="Component",
    )
    binary = fields.Boolean(string="Is Binary")

    path_name = fields.Char(
        compute="compute_path_name",
        store=True,
        string="Path name"
    )

    @api.multi
    @api.depends('name', 'binary')
    def compute_path_name(self):
        for obj in self:
            if obj.binary:
                obj.path_name = 'binary-%s' % obj.name
            else:
                obj.path_name = obj.name

    @api.multi
    def get_release(self):
        self.ensure_one()

        component = self.component_id
        distribution = component.distribution_id
        name = self.name

        data = """Archive: %(archive)s
Version: %(version)s
Component: %(component)s
Origin: %(origin)s
Label: %(label)s
Architecture: %(architecture)s
""" % {
            "archive": distribution.name,
            "version": distribution.version,
            "component": component.name,
            "origin": distribution.origin,
            "label": distribution.label,
            "architecture": name
        }
        return data.encode()

    @api.multi
    def get_packages(self):
        self.ensure_one()

        return b'\n\n'.join([
            package.description.encode()
            for package
            in self.package_ids
        ])

    @api.multi
    def get_packages_xz(self):
        self.ensure_one()
        data = self.get_packages()
        return lzma.compress(data)

    @api.multi
    def get_packages_gz(self):
        self.ensure_one()
        data = self.get_packages()
        buf = BytesIO()

        # TODO use the latest timestamp of all of the packages
        mtime = self.write_date
        dt = datetime.strptime(mtime, DEFAULT_SERVER_DATETIME_FORMAT)
        mtime = dt.timestamp()

        with gzip.GzipFile(
            fileobj=buf,
            mode='wb',
            compresslevel=9,
            mtime=mtime
        ) as fgz:
            fgz.write(data)

        return buf.getvalue()


class DebianPackage(models.Model):
    _name = 'debian.package'
    name = fields.Char(string="Name")
    architecture_ids = fields.Many2many(
        'debian.architecture',
        'debian_package_debian_architecture_rel',
        'package_id',
        'architecture_id',
        string='Architectures'
    )

    debfile = fields.Binary(string="debfile")
    filename = fields.Char(string="Filename")
    description = fields.Text(
        compute='compute_description',
        store=True,
        string="Description"
    )

    @api.multi
    def extract_control(self, debfile):
        datab = BytesIO(debfile)

        arfile = ArFile(fileobj=datab)

        control = arfile.getmember('control.tar.gz')
        # Hack bug in API works only with __fname defined
        control._ArMember__fp = datab
        datab.seek(control._ArMember__offset)

        control_data = b''

        while True:
            new_data = control.read(1)
            control_data += new_data
            if not new_data:
                break

        # control_buf = BytesIO(control_data)
        control_buf = BytesIO(gzip.decompress(control_data))
        control_tar = tarfile.open(fileobj=control_buf, mode='r')
        member = control_tar.getmember('./control')
        offset = member.offset_data
        size = member.size

        control_buf.seek(offset)
        content = control_buf.read(size)

        return content.decode()

    @api.multi
    def parse_control(self, control):
        res = []
        for line in control.split('\n'):

            if len(line) > 0 and line[0] != ' ':
                parts = line.split(':', 1)

                if len(parts) == 2:
                    res.append([parts[0], parts[1]])

        return res

    @api.multi
    @api.depends('debfile')
    def compute_description(self):
        for obj in self:
            if not obj.debfile:
                obj.description = ''
                continue

            data = b64decode(obj.debfile)
            control = obj.extract_control(data)
            args = obj.parse_control(control)

            args.append([
                'Size', str(len(data))
            ])

            args.append([
                'Filename', path.join('pool/p', str(obj.id), obj.filename)
            ])

            for name, checksum in checksum_func.items():

                if name == 'MD5Sum':
                    name = 'MD5sum'

                args.append([
                    name, checksum(data)
                ])

            lines = ["%s: %s" % (key, value) for key, value in args]

            obj.description = "\n".join(lines)


class DebianDistribution2(models.Model):
    _inherit = 'debian.distribution'

    component_ids = fields.One2many(
        'debian.component',
        'distribution_id',
        string="Components"
    )


class DebianComponent2(models.Model):
    _inherit = 'debian.component'

    architecture_ids = fields.One2many(
        'debian.architecture',
        'component_id',
        string="Architectures"
    )


class DebianArchitecture2(models.Model):
    _inherit = 'debian.architecture'

    package_ids = fields.Many2many(
        'debian.package',
        'debian_package_debian_architecture_rel',
        'architecture_id',
        'package_id',
        string='Packages'
    )

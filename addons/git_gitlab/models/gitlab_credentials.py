# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
import gitlab
import logging

log = logging.getLogger(__name__)


class GitlabCredentials(models.Model):
    _name = 'gitlab.credentials'

    name = fields.Char(string="Username")
    host = fields.Char(string="Hostname")
    token = fields.Char(string="Token")

    create_mr = fields.Boolean(string="Create MR")
    clone_projects = fields.Boolean(string="Clone Projects")

    @api.model
    def get_first(self, kwargs=None):
        if kwargs is not None:
            domain = [
                (key, '=', value)
                for key, value in kwargs.items()
            ]
        else:
            domain = []

        return self.search(domain, limit=1)

# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
import gitlab
import logging

log = logging.getLogger(__name__)


class GitPushEvent(models.Model):
    _inherit = 'git.event.push'

    @api.model
    def create(self, vals):
        res = super(GitPushEvent, self).create(vals)

        if (
            not res.backend == 'gitlab' or
            res.after_commit_id == res.NULL_COMMIT
        ):
            return res

        cred = self.env['gitlab.credentials'].get_first(create_mr=True)
        if not cred:
            return res

        try:
            log.info('Log in gitlab')
            gl = gitlab.Gitlab(cred.host, cred.token)
            gl.auth()

            log.info('Load project with id %d' % res.project_id.gitlab_id)
            project = gl.projects.get(res.project_id.gitlab_id)
            source_branch = res.ref.rsplit('/', 1)[1]
            target_branch = res.project_id.get_target_branch(source_branch)

            if not target_branch:
                return res

            log.info('Search for mr for  %s' % source_branch)
            mrs = project.mergerequests.list(
                state='opened',
                source_branch=source_branch
            )
            log.info('Found %d mr' % len(mrs))

            if not len(mrs) and source_branch != target_branch:
                mr = project.mergerequests.create({
                    'source_branch': source_branch,
                    'target_branch': target_branch,
                    'title': 'WIP: %s -> %s' % (source_branch, target_branch),
                })
                log.info('Created mr with name %s' % mr.title)
        except Exception as exc:
            log.info(exc)

        return res

# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
import gitlab
import logging

log = logging.getLogger(__name__)


class GitMergeRequestEvent(models.Model):
    _inherit = 'git.event.merge_request'

    @api.multi
    def write(self, vals):
        stage_map = {}

        for obj in self:
            if (
                'stage_id' in vals and
                vals['stage_id'] != obj.stage_id.id
            ):
                stage_map[obj.id] = obj.stage_id.id

        res = super(GitMergeRequestEvent, self).write(vals)

        if not stage_map or self.env.context.get('GITLAB_WEBHOOK'):
            return res

        cred = self.env['gitlab.credentials'].get_first()
        gl = gitlab.Gitlab(cred.host, cred.token)
        gl.auth()

        for key, value in stage_map.items():
            if not obj.backend == 'gitlab':
                continue

            obj = self.browse(key)
            project = gl.projects.get(self.source_project_id.gitlab_id)
            mr = project.mergerequests.get(obj.gitlab_iid)

            if obj.stage_id.code == 'merged':
                mr.merge(
                    should_remove_source_branch=True,
                    merge_when_pipeline_succeeds=True,
                )
                mr.notes.create({
                    "body": "Merged by %s" % self.env.user.login
                })
            elif obj.stage_id.code == 'opened':
                mr.state_event = 'reopen'
                mr.title = 'WIP:%s' % mr.title.replace('WIP:', '')
                mr.save()
                mr.notes.create({
                    "body": (
                        "Merge request reoponed by %s" % self.env.user.login
                    )
                })
            elif obj.stage_id.code == 'closed':
                mr.state_event = 'close'
                mr.save()
                mr.notes.create({
                    "body": (
                        "Merge request closed by %s" % self.env.user.login
                    )
                })
            elif (
                obj.stage_id.code == 'ready' and
                mr.title.startswith('WIP:')
            ):
                mr.title = mr.title.replace('WIP:', '')
                mr.save()
                mr.notes.create({
                    "body": (
                        "Merge request marked as ready by %s" % (
                            self.env.user.login
                        )
                    )
                })

        return res

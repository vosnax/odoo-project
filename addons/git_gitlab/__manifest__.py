# -*- coding: utf-8 -*-
{
    'name': 'Git Gitlab',
    'version': '11.0.0.1.0',
    'author': 'Loïc Faure-Lacroix',
    'maintainer': '',
    'website': 'http://archeti.ca',
    'license': 'AGPL-3',
    'category': 'Others',
    'summary': '',
    'description': """
""",
    'depends': [
        'base',
        'git_webhooks',
        'git_webhooks_gitlab',
    ],
    'external_dependencies': {
        'python': ['gitlab'],
    },
    'data': [
        'views/views.xml',
        'actions/actions.xml',
        'menus/menus.xml',
        'security/acl.xml'
    ],
    'installable': True,
    'application': True,
}
